﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace appDomain.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var x = System.Diagnostics.Process.GetCurrentProcess().Id;

            var y = AppDomain.CurrentDomain.Id;


            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}