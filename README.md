# README #

This is part of IoT Series, where I am trying to build few IoT devices that push events to Azure EventHub.  From the EventHub, Azure Stream Analytics will execute my query to calculate average values for each individual device and publish these average values to Azure ServiceBus. From Azure ServiceBus, I am going to read the average values in Azure Functions Apps and save them into Azure Redis Cache. My Azure Website will poll this Redis Cache and displays the average values.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact