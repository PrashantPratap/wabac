﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace BasicAuth
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }


        private static Lazy<ConnectionMultiplexer> lazyConnection = new Lazy<ConnectionMultiplexer>(() =>
        {
            string cacheConnection = "wabac.redis.cache.windows.net:6380,password=+nnv0pEutXPeIGuClf8zWXoY7I9U1fCvVjTvjDA4wsE=,ssl=True,abortConnect=False";
            return ConnectionMultiplexer.Connect(cacheConnection);
        });

        private static ConnectionMultiplexer Connection
        {
            get
            {
                return lazyConnection.Value;
            }
        }

        public static void LogMessage(string msg)
        {
            int pid = Process.GetCurrentProcess().Id;
            int aid = AppDomain.CurrentDomain.Id;
            IDatabase cache = Connection.GetDatabase();

            cache.ListRightPush("WebHookTest", $"{DateTime.Now}-{pid}-{aid}-{msg}");

        }
    }
}
