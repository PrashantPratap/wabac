﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BasicAuth.Models
{
    public class BasicAuthenticationAttribute : ActionFilterAttribute
    {
       public string BasicRealm { get; set; }
        protected string Username { get; set; }
        protected string Password { get; set; }

        public BasicAuthenticationAttribute(string username, string password)
        {
            this.Username = username;
            this.Password = password;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            MvcApplication.LogMessage($"Got a HTTP {filterContext.RequestContext.HttpContext.Request.RequestType} request with {filterContext.RequestContext.HttpContext.Request.RawUrl}");

            var sr = new StreamReader(filterContext.RequestContext.HttpContext.Request.InputStream);
            string content = sr.ReadToEnd();

            MvcApplication.LogMessage(content);

            var req = filterContext.HttpContext.Request;
            var auth = req.Headers["Authorization"];
            if (!String.IsNullOrEmpty(auth))
            {
                MvcApplication.LogMessage($"Authorization Header : {auth}");
            
                var cred = System.Text.ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(auth.Substring(6))).Split(':');
                var user = new { Name = cred[0], Pass = cred[1] };

                MvcApplication.LogMessage($"Username : {user.Name} Password : {user.Pass}");

                if (user.Name == Username && user.Pass == Password)
                {
                    MvcApplication.LogMessage("Basic Auth worked");
                    return;
                }
                else
                {
                    MvcApplication.LogMessage("Basic Auth failed");
                }
            }
            else
            {
                MvcApplication.LogMessage("Authorization header is empty");
            }
            filterContext.HttpContext.Response.AddHeader("WWW-Authenticate", String.Format("Basic realm=\"{0}\"", BasicRealm ?? "ppratap"));
            /// thanks to eismanpat for this line: http://www.ryadel.com/en/http-basic-authentication-asp-net-mvc-using-custom-actionfilter/#comment-2507605761
            filterContext.Result = new HttpUnauthorizedResult();
        }
    }
}