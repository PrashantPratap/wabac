﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FunctionKustoEvents
{
    class Program
    {
        class OurRow
        {
            public DateTime startTime;
            public DateTime endTime;
            public Guid id;
            public string functionName;
            public TimeSpan duration;
        }
        static void Main(string[] args)
        {
            DateTime dt = DateTime.Now;
            string[] currentRow = null;
            string summary = string.Empty, strID = string.Empty, funcitonName = string.Empty;
            Guid id;
            int indexstart = 0;
            int indexend = 0;

            Dictionary<Guid, OurRow> pending = new Dictionary<Guid, OurRow>();
            List<OurRow> done = new List<OurRow>();
            OurRow r = null;

            string inputfilename = @"d:\temp\af\Function_SB.csv";
            string outputfilename = inputfilename + "_report2.csv";

            var files = System.IO.Directory.EnumerateFiles(@"d:\temp\af").Where( x => x.Contains("Function_SB_"));
            foreach (var f in files)
            {
                Console.WriteLine($"file name is {f}. Done count is {done.Count()}. Pending count is {pending.Count()}");

                var csv = new Microsoft.VisualBasic.FileIO.TextFieldParser(f);

                csv.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited;
                csv.SetDelimiters(",");



                while (!csv.EndOfData)
                {
                    currentRow = csv.ReadFields();

                    //TIMESTAMP	FunctionName	Summary
                    //2017 - 04 - 06 17:35:00.0000477 EntitySave Function started(Id = f9b8a99e - 79cc - 4c8d - b9c6 - 5f62b69a3d27)

                    if (DateTime.TryParse(currentRow[0], out dt))
                    {
                        summary = currentRow[17];

                        if (!string.IsNullOrEmpty(summary))
                        {
                            // 36
                            indexstart = summary.IndexOf("Id=");
                            if (indexstart == -1)
                            {
                                continue;
                            }
                            strID = summary.Substring(indexstart + 3, 36);

                            if (!Guid.TryParse(strID, out id))
                            {
                                continue;
                            }

                            indexstart = summary.IndexOf("'Functions.");
                            if (indexstart == -1)
                            {
                                continue;
                            }
                            indexend = summary.IndexOf("'", indexstart + 1);
                            if (indexend == -1)
                            {
                                continue;
                            }
                            funcitonName = summary.Substring(indexstart + 11, indexend - (indexstart + 11));

                            if (summary.StartsWith("Executing"))
                            {
                                pending.Add(id, new OurRow() { startTime = dt, functionName = funcitonName });
                            }
                            else if (summary.StartsWith("Executed"))
                            {
                                if (pending.TryGetValue(id, out r))
                                {
                                    r.endTime = dt;
                                    r.duration = r.endTime.Subtract(r.startTime);

                                    pending.Remove(id);
                                    done.Add(r);
                                }
                            }
                        }
                    }
                }
            }

            StreamWriter sw = new StreamWriter(outputfilename);
            sw.WriteLine("this is a test");
            
            foreach (var d in done)
            {
                sw.WriteLine($"{d.functionName},{d.duration.TotalMilliseconds},{d.functionName},{d.startTime},{d.endTime}");
            }
            sw.Close();
            /*
             TIMESTAMP	FunctionName	Summary
2017-04-06 17:35:00.0124867		Executing 'Functions.EntitySave' (Reason='New ServiceBus message detected on 'prod-entitysave/Subscriptions/Audit'.', Id=414001bc-44d1-41bb-9571-57a0c5874003)
2017-04-06 17:35:00.0126877	EntitySave	Function started (Id=414001bc-44d1-41bb-9571-57a0c5874003)
2017-04-06 17:35:00.0401109	EntitySave	Function completed (Success, Id=414001bc-44d1-41bb-9571-57a0c5874003)
2017-04-06 17:35:00.0403425		Executed 'Functions.EntitySave' (Succeeded, Id=414001bc-44d1-41bb-9571-57a0c5874003)

        TIMESTAMP	FunctionName	Summary
2017-04-06 17:54:59.8259721		Executed 'Functions.IndexUpdate' (Succeeded, Id=7da44ca4-b1c9-49cf-9f9f-d2602a3b656a)
2017-04-06 17:54:59.8256394	IndexUpdate	Function completed (Success, Id=7da44ca4-b1c9-49cf-9f9f-d2602a3b656a)
2017-04-06 17:54:59.8098973	IndexUpdate	Function started (Id=7da44ca4-b1c9-49cf-9f9f-d2602a3b656a)
2017-04-06 17:54:59.8066418		Executing 'Functions.IndexUpdate' (Reason='New ServiceBus message detected on 'prod-indexupdate/Subscriptions/Audit'.', Id=7da44ca4-b1c9-49cf-9f9f-d2602a3b656a)

             */

        }
    }
}
