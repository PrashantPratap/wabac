// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the PINVOKEC_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// PINVOKEC_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef PINVOKEC_EXPORTS
#define PINVOKEC_API __declspec(dllexport)
#else
#define PINVOKEC_API __declspec(dllimport)
#endif

// This class is exported from the PInvokeC.dll
class PINVOKEC_API CPInvokeC {
public:
	CPInvokeC(void);
	// TODO: add your methods here.
};

extern PINVOKEC_API int nPInvokeC;

PINVOKEC_API int fnPInvokeC(void);


#ifndef MYAPI
#define MYAPI 
#endif

#ifdef __cplusplus
extern "C" {
#endif

	// Exported functions go here.
	MYAPI void print_line(const char* str);
	MYAPI int MyAdd(int a, int b);
	

#ifdef __cplusplus
}
#endif