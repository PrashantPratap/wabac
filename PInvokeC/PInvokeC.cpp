// PInvokeC.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "PInvokeC.h"


// This is an example of an exported variable
PINVOKEC_API int nPInvokeC=0;

// This is an example of an exported function.
PINVOKEC_API int fnPInvokeC(void)
{
    return 42;
}

// This is the constructor of a class that has been exported.
// see PInvokeC.h for the class definition
CPInvokeC::CPInvokeC()
{
    return;
}


MYAPI void print_line(const char* str) {
	//printf("%s\n", str);
}

MYAPI int MyAdd(int a, int b)
{
	return (a + b);
}