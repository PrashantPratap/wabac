using System;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using StackExchange.Redis;

namespace FunctionAppRedisCacheTimer
{
    public static class FunctionTimer
    {
        [FunctionName("FunctionTimer")]
        public static void Run([TimerTrigger("0 */1 * * * *")]TimerInfo myTimer, TraceWriter log)
        {
            log.Info($"C# Timer trigger function executed at: {DateTime.Now}");

            IDatabase cache = Connection.GetDatabase();
            var name = cache.ListRightPop("KEY_NAME");

            if (name.HasValue)
            {
                log.Info($"from redis cache - {name}");
            }
            else
            {
                log.Info($"no items found in redis cache");
            }
        }

        private static string REDIS_CONNECTIONSTRING =
            System.Environment.GetEnvironmentVariable(
                "REDIS_CONNECTIONSTRING", EnvironmentVariableTarget.Process);

        private static Lazy<ConnectionMultiplexer> lazyConnection = new Lazy<ConnectionMultiplexer>(() =>
        {
            return ConnectionMultiplexer.Connect(REDIS_CONNECTIONSTRING);
        });
        public static ConnectionMultiplexer Connection
        {
            get
            {
                return lazyConnection.Value;
            }
        }
    }
}
