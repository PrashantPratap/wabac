# How to send msg between Azure Function via Redis Cache

HTTP Trigger (FunctionHTTP.cs) function pushes message to Redis Cache

Timer Trigger (FunctionTimer.cs) function pull messages from Redis Cache

# Steps

1. Create Redis Cache in Azure Portal, click Create a resource | Databases | Redis Cache. [Tutorial](https://docs.microsoft.com/en-us/azure/redis-cache/cache-dotnet-how-to-use-azure-redis-cache#create-a-cache)
2. [Copy Redis Connection String](https://docs.microsoft.com/en-us/azure/redis-cache/cache-dotnet-how-to-use-azure-redis-cache#retrieve-host-name-ports-and-access-keys-using-the-azure-portal)
3. Add function app setting REDIS_CONNECTIONSTRING and set the value to above connection string
