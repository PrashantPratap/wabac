﻿using Microsoft.ServiceBus.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IoT
{
    class Program
    {
        static void Main(string[] args)
        {
            SendEvents(100);
        }

        static void SendEvents(int count)
        {
            //var eventHubName = "fapp";
            //var connectionString = "Endpoint=sb://wabaceventhub.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=5FzFWyeyovd18+/hQENEKH5TEPnbCPKKb6xN7Kp7N6I=";
            

            var eventHubName = "testeventhub";
            var connectionString = "Endpoint=sb://binisha.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=/DB7EsfnJBzCqA5JjZGQ29cdUkVNbm/HtPlEQ+2u79I=";

            var eventHubClient = EventHubClient.CreateFromConnectionString(connectionString, eventHubName);

            Console.Write($"Sending {count} messages");

            MyInput e = new MyInput();

            Random r = new Random();

            for (int i = 0; i < count; i++)
            {
                e.DeviceName = $"Console App - {i}";
                e.DateTime = DateTime.Now.ToString("o");
                e.temperature = r.Next(0, 100);
                e.speed = r.Next(200, 300);

                var msg = Newtonsoft.Json.JsonConvert.SerializeObject(e);
                //var msg = $"Event ({i}) from console app at {DateTime.Now.ToString("o")}";
                eventHubClient.Send(new EventData(Encoding.UTF8.GetBytes(msg)));
                Console.Write(".");
                System.Threading.Thread.Sleep(1000);
            }
            Console.WriteLine("done");
        }

        class MyInput
        {
            public string DateTime { get; set; }
            public string DeviceName { get; set; }
            public int temperature { get; set; }
            public int speed { get; set; }
        }
        class MyOutput
        {
            //{"devicename":"Console App - 8","avgtemp":62.0,"avgspeed":207.0}
            public string devicename { get; set; }
            public double avgtemp { get; set; }
            public double avgspeed { get; set; }
        }

    }
}
