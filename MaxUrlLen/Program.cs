﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MaxUrlLen
{
    class Program
    {
        static void Main(string[] args)
        {
            HttpClient c = new HttpClient();

            for (int i = 0; i < 10000; i++)
            {
                var str = new string('a', i);
                var t =  c.GetAsync($"http://wabac.azurewebsites.net/?{str}");
                t.Wait();
                var r = t.Result;
               if(r.IsSuccessStatusCode)
                {
                    Console.WriteLine($"{i} is OK");
                }
               else
                    {
                    Console.WriteLine($"{i} is {r.StatusCode}");
                }
            }
            
        }
    }
}
