﻿using Microsoft.Azure.Devices.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IoTHub
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("IoT Hub Quickstarts #1 - Simulated device. Ctrl-C to exit.\n");
            
            // Connect to the IoT hub using the MQTT protocol

            s_deviceClient = DeviceClient.CreateFromConnectionString(s_connectionString, TransportType.Mqtt);

            SendDeviceToCloudMessagesAsync();

            Console.ReadLine();
        }

        private static DeviceClient s_deviceClient;
        private readonly static string s_connectionString = "HostName=AIError.azure-devices.net;DeviceId=OneDevice;SharedAccessKey=H5uqz1QH1i3IOX0MpBrsXJDwUoiePSn2V5+72qaa18c=";



        // Async method to send simulated telemetry
        private static async void SendDeviceToCloudMessagesAsync()
        {
            // Initial telemetry values
            double minTemperature = 20;
            double minHumidity = 60;

            Random rand = new Random();
            
            while (true)
            {
                double currentTemperature = minTemperature + rand.NextDouble() * 15;
                double currentHumidity = minHumidity + rand.NextDouble() * 20;

                // Create JSON message
                var telemetryDataPoint = new
                {
                    temperature = currentTemperature,
                    humidity = currentHumidity
                };
                var messageString = JsonConvert.SerializeObject(telemetryDataPoint);
               // messageString = messageString + "}";
                var message = new Message(Encoding.ASCII.GetBytes(messageString));

                message.Properties.Add("temperatureAlert", (currentTemperature > 30) ? "true" : "false");
                
                // Send the telemetry message
                await s_deviceClient.SendEventAsync(message);
                Console.WriteLine("{0} > Sending message: {1}", DateTime.Now, messageString);

               // Console.Read();

                //if( Console.ReadLine() == "C" || Console.ReadLine() == "c")
                //{
                //    break;
                //}       
            }
        }
    }
}
