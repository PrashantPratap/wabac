﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace MyBufferBlock
{
    class Program
    {
        static void Main(string[] args)
        {
            string FunctionName = "one";

            var bufferBlock = new BufferBlock<(MyData data, ILogger log)>(new DataflowBlockOptions { MaxMessagesPerTask = 1, });

            var action = new ActionBlock<(MyData data, ILogger log)>(async item =>
            {
                await MyLogic.RunAsync(item.data, item.log, FunctionName);

            },
                new ExecutionDataflowBlockOptions
                {
                    MaxDegreeOfParallelism = 10
                });

            bufferBlock.LinkTo(action, new DataflowLinkOptions { PropagateCompletion = true });


            for(int i=0;i<100;i++)
            {   
                MyData d = new MyData() { d= i};
                bufferBlock.Post((d,null));
            }

           // bufferBlock.Complete();
           // bufferBlock.Completion.Wait();
        }


    }
    interface ILogger
    {

    }

    class MyData
    {
        public int d { get; set; }
    }

    class MyLogic
    {
        public static async Task<string> RunAsync(MyData data, ILogger log, string fName)
        {
            int tid = System.Threading.Thread.CurrentThread.ManagedThreadId;
            Console.WriteLine($"on Thread ({tid}) processing data ({data.d})");
            return string.Empty;
        }
    }

}
