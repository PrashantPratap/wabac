﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace SlotSwapTest2
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            LogMessage("Application_Start");
        }
        
        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            var context = HttpContext.Current;
            var msg = $"Application_BeginRequest-{context.Request.Url}-{context.Request.ServerVariables["SERVER_NAME"]}-{context.Request.ServerVariables["HTTP_USER_AGENT"]}";
            LogMessage(msg);


            context.Response.Clear();
            context.Response.StatusCode = 200;
            context.Response.Write(msg);
            context.Response.Flush();
            context.Response.SuppressContent = true;
            context.ApplicationInstance.CompleteRequest();
        }

        
        protected void Application_End(object sender, EventArgs e)
        {
            LogMessage("Application_End");
        }

        private static Lazy<ConnectionMultiplexer> lazyConnection = new Lazy<ConnectionMultiplexer>(() =>
        {
            string cacheConnection = "wabac.redis.cache.windows.net:6380,password=+nnv0pEutXPeIGuClf8zWXoY7I9U1fCvVjTvjDA4wsE=,ssl=True,abortConnect=False";
            return ConnectionMultiplexer.Connect(cacheConnection);
        });

        private static ConnectionMultiplexer Connection
        {
            get
            {
                return lazyConnection.Value;
            }
        }

        public static void LogMessage(string msg)
        {
            int pid = Process.GetCurrentProcess().Id;
            int aid = AppDomain.CurrentDomain.Id;

            string localCache = Environment.GetEnvironmentVariable("WEBSITE_LOCAL_CACHE_OPTION");
            string localCacheReady = Environment.GetEnvironmentVariable("WEBSITE_LOCALCACHE_READY");
            //string key = $"{pid}-{aid}";
            IDatabase cache = Connection.GetDatabase();

            cache.ListLeftPush("LocalCacheTest", $"{DateTime.Now}-{pid}-{aid}-{localCache}-{localCacheReady}-{msg}");

            //if(cache.ListLength("LocalCacheTest") > 500 )
            //{
            //    for (int i = 0; i < 100; i++)
            //        cache.ListRightPop("LocalCacheTest");
            //}

        }
    }
}