﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Core502.Controllers
{
    public class HomeController : Controller
    {
        static System.Threading.ManualResetEventSlim mres = new System.Threading.ManualResetEventSlim(false);
        static int TimeoutInMS = 5000;
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }

        public IActionResult Timeout(int id)
        {
            if (id != 0)
            {
                TimeoutInMS = id;
            }
            ViewBag.Timeout = TimeoutInMS;
            return View();
        }

        public IActionResult LoadTestPage1()
        {
            mres.Wait(TimeoutInMS);
            return View();
        }

        public IActionResult LoadTestPage2()
        {
            mres.Wait(TimeoutInMS);
            return View();
        }

        public IActionResult LoadTestPage3()
        {
            mres.Wait(TimeoutInMS);
            return View();
        }
    }
}
