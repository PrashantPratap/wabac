using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;

namespace DurableFunctionSlot
{
    public static class MainFunction
    {
        private static string DllVersion()
        {
            var machineName = Environment.GetEnvironmentVariable("COMPUTERNAME");
            var instanceID = Environment.GetEnvironmentVariable("WEBSITE_INSTANCE_ID");

            int pid = System.Diagnostics.Process.GetCurrentProcess().Id;
            int domainid = System.AppDomain.CurrentDomain.Id;


            Assembly thisAssem = typeof(MainFunction).Assembly;
            AssemblyName thisAssemName = thisAssem.GetName();
            DateTime dllTime = System.IO.File.GetLastWriteTime(thisAssem.Location);


            return $"DllTime STAGING is {dllTime} ; PID is {pid} ; AppDomainID is {domainid} ; machineName is {machineName} ; instanceID is {instanceID}";
            
        }

        /*
         * It looks like this is expected behavior if both slots use same storage account and same task hub name. Here are details https://docs.microsoft.com/en-us/azure/azure-functions/durable-functions-task-hubs 
Can you please check if your slots are using same storage account and the host.json has same task hub name. 

         */
        [FunctionName("MainFunction")]
        public static async Task<List<string>> RunOrchestrator(
            [OrchestrationTrigger] DurableOrchestrationContext context)
        {
            var outputs = new List<string>();

            outputs.Add($"Start-{DllVersion()}");

            // Replace "hello" with the name of your Durable Activity Function.
            outputs.Add(await context.CallActivityAsync<string>("MainFunction_Hello", "Tokyo"));
            //outputs.Add(await context.CallActivityAsync<string>("MainFunction_Hello", "Seattle"));
            //outputs.Add(await context.CallActivityAsync<string>("MainFunction_Hello", "London"));

            string strApprovalEvent = await context.WaitForExternalEvent<string>("ApprovalEvent");

            if (strApprovalEvent == "true")
            {
                outputs.Add("Approved");
            }
            else
            {
                outputs.Add("Rejected");
            }

            outputs.Add($"End-{DllVersion()}");
            // returns ["Hello Tokyo!", "Hello Seattle!", "Hello London!"]
            return outputs;
        }

        [FunctionName("MainFunction_Hello")]
        public static string SayHello([ActivityTrigger] string name, ILogger log)
        {
            var dllVersion = DllVersion();
            log.LogInformation($"Saying hello to {name}-{dllVersion}.");
            return $"Hello {name}-{dllVersion}!";
        }

        [FunctionName("MainFunction_HttpStart")]
        public static async Task<HttpResponseMessage> HttpStart(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post")]HttpRequestMessage req,
            [OrchestrationClient]DurableOrchestrationClient starter,
            ILogger log)
        {
            // Function input comes from the request content.
            string instanceId = await starter.StartNewAsync("MainFunction", null);

            log.LogInformation($"Started orchestration with ID = '{instanceId}'.-{DllVersion()}");

            return starter.CreateCheckStatusResponse(req, instanceId);
        }
    }
}