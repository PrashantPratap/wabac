﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeZone
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime utc = DateTime.UtcNow;
            TimeZoneInfo zone = TimeZoneInfo.FindSystemTimeZoneById("Pacific SA Standard Time");
            DateTime localDateTime = TimeZoneInfo.ConvertTimeFromUtc(utc, zone);
            Console.WriteLine("UTC time: " + utc);
            Console.WriteLine("Santiago time:  " + localDateTime + $" {zone.DisplayName}");

            bool isDaylight = zone.IsDaylightSavingTime(localDateTime);
            if (isDaylight) Console.WriteLine($"{zone.DaylightName}");
            else
                Console.WriteLine($"{zone.DisplayName}");

            foreach (TimeZoneInfo z in TimeZoneInfo.GetSystemTimeZones())
            {
                TimeZoneInfo x = TimeZoneInfo.FindSystemTimeZoneById(z.Id);
                DateTime time = TimeZoneInfo.ConvertTimeFromUtc(utc, x);
                

                Console.WriteLine( $"time {time}" +x.Id + " :: " + x.BaseUtcOffset + " :: " + x.DaylightName + " :: " + x.SupportsDaylightSavingTime);

            }



zone = TimeZoneInfo.FindSystemTimeZoneById("Pacific SA Daylight Time");
            localDateTime = TimeZoneInfo.ConvertTimeFromUtc(utc, zone);
            Console.WriteLine("UTC time: " + utc);
            Console.WriteLine("Santiago time:  " + localDateTime);

        }
    }
}
