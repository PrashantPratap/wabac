﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using dotnetNode.Models;
using Microsoft.AspNetCore.NodeServices;

namespace dotnetNode.Controllers
{
    public class HomeController : Controller
    {
        INodeServices nodeServices;
        public HomeController(INodeServices nodeServices)
        {
            this.nodeServices = nodeServices;
        }
        public IActionResult Index()
        {
            return View();
        }

public async  Task<IActionResult> About()
{
    var msg = await nodeServices.InvokeAsync<string>("./hello.js", 1);

    ViewData["Message"] = msg;
    return View();
}

        public async Task<IActionResult> Contact()
        {
            var msg = await nodeServices.InvokeAsync<string>(@"./NodeSrc/hello2.js", 1);
            ViewData["Message"] = msg;

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
