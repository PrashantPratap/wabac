﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient; // Nuget Package name is : System.Data.SqlClient
using Microsoft.Extensions.Configuration;

namespace dotnetCore.Controllers
{
    public class HomeController : Controller
    {
        // Place to store the Config object and use in this controller
        private readonly IConfiguration Config;

        public HomeController(IConfiguration config)
        {
            Config = config;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            try
            {
                string ConnectionString = string.Empty;

                var str = Config["DBConnectionString"];
                if (!string.IsNullOrWhiteSpace(str))
                {
                    ConnectionString = str;
                }
                var conn = new SqlConnection(ConnectionString);
                conn.Open();

                using (SqlCommand cmd = new SqlCommand(@"SELECT SUSER_SNAME()", conn))
                {
                    ViewData["Message"] = DateTime.Now.ToString() +  " : You have successfully logged on as: " + (string)cmd.ExecuteScalar();
                }
            }
            catch(Exception ex)
            {
                ViewData["Message"] = ex.ToString();
            }
            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
