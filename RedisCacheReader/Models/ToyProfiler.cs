﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RedisCacheReader.Models
{
    class ToyProfiler : IProfiler
    {
        public object thisAppContext = new object();
        public object GetContext()
        {
            return thisAppContext;
        }
        public static ToyProfiler profiler = new ToyProfiler();
    }
}