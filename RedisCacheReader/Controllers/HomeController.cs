﻿using RedisCacheReader.Models;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace RedisCacheReader.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
           // Response.AddHeader("Refresh", "5");
            ViewBag.Message = "Your application description page.";

            IDatabase cache = Connection.GetDatabase();

            List<SelectListItem> items = new List<SelectListItem>();

            var urls = cache.ListRange("URLs", 0,- 1);
            foreach(var url in urls)
            {
                items.Add(new SelectListItem() { Text = url });
            }


            IEnumerable<IProfiledCommand> timings = Connection.FinishProfiling(ToyProfiler.profiler.thisAppContext);
            Connection.BeginProfiling(ToyProfiler.profiler.thisAppContext);

            var x = Connection.GetStatus();


            foreach (var item in timings)
            {
                items.Add(new SelectListItem() { Text = item.ToString() });
            }

            ViewBag.URLs = items;

            return View();
        }

        public ActionResult Clear()
        {
            IDatabase cache = Connection.GetDatabase();

            cache.KeyDelete("URLs");

            return RedirectToAction("About");
        }

        public ActionResult Contact()
        {
            IDatabase cache = Connection.GetDatabase();
            cache.ListLeftPush("URLs", DateTime.UtcNow.ToLongTimeString());
            ViewBag.Message = "Your contact page.";

            return View();
        }

        static StringWriter sw = new StringWriter();
        private static Lazy<ConnectionMultiplexer> lazyConnection = new Lazy<ConnectionMultiplexer>(() =>
        {
            StackExchange.Redis.ConfigurationOptions options = new ConfigurationOptions();
            options.ClientName = $"{System.Environment.GetEnvironmentVariable("COMPUTERNAME", EnvironmentVariableTarget.Process)}-<function name>-{DateTime.UtcNow}";
            options.EndPoints.Add("wabac.redis.cache.windows.net");
            options.Ssl = true;
            options.Password = "+nnv0pEutXPeIGuClf8zWXoY7I9U1fCvVjTvjDA4wsE=";
            var connection = ConnectionMultiplexer.Connect(options);

            //string cacheConnection = "wabac.redis.cache.windows.net:6380,password=+nnv0pEutXPeIGuClf8zWXoY7I9U1fCvVjTvjDA4wsE=,ssl=True,abortConnect=False";
            //var conn = ConnectionMultiplexer.Connect(cacheConnection,sw);
            var conn = ConnectionMultiplexer.Connect(options,sw);

            conn.RegisterProfiler(ToyProfiler.profiler);

            conn.BeginProfiling(ToyProfiler.profiler.thisAppContext);

            
            return conn;
        });

        private static ConnectionMultiplexer Connection
        {
            get
            {
                return lazyConnection.Value;
            }
        }
    }
}