﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace MyCoreTest.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }

        static System.Threading.ManualResetEventSlim mres = new System.Threading.ManualResetEventSlim(false);
        static int TimeoutInMS = 5000;

        public IActionResult Timeout(int id)
        {
            if (id != 0)
            {
                TimeoutInMS = id;
            }
            ViewBag.Timeout = TimeoutInMS;
            return View();
        }

        public IActionResult LoadTestPage1()
        {
            ViewBag.StartTime = DateTime.Now;
            mres.Wait(TimeoutInMS);
            ViewBag.Duration = DateTime.Now.Subtract(ViewBag.StartTime).TotalMilliseconds;
            return View();
        }

        public IActionResult LoadTestPage2()
        {
            ViewBag.StartTime = DateTime.Now;
            mres.Wait(TimeoutInMS);
            ViewBag.Duration = DateTime.Now.Subtract(ViewBag.StartTime).TotalMilliseconds;
            return View();
        }

        public IActionResult LoadTestPage3()
        {
            ViewBag.StartTime = DateTime.Now;
            mres.Wait(TimeoutInMS);
            ViewBag.Duration = DateTime.Now.Subtract(ViewBag.StartTime).TotalMilliseconds;
            return View();
        }
    }
}
