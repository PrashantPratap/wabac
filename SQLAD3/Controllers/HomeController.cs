﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SQLAD3.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {

            //using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
            //{
            //    try
            //    {
            //        connection.AccessToken = accessToken;
            //        connection.Open();
            //        Console.WriteLine("Connected to the database");
            //    }
            //    catch (Exception ex)
            //    {
            //        Console.WriteLine(ex.Message);
            //    }
            //}


            //HttpContext.GetOwinContext().TraceOutput.
            
            
            try
            {
                string ConnectionString = @"Data Source=adtestwabac.database.windows.net; Authentication=Active Directory Integrated; Initial Catalog=adtest;";
                SqlConnection conn = new SqlConnection(ConnectionString);

                conn.AccessToken = Request.Headers["X-MS-TOKEN-AAD-ID-TOKEN"];
                conn.Open();

                SqlCommand comm = new SqlCommand("Select * from Persons", conn);
                var dr = comm.ExecuteReader();

                string str = string.Empty;
                while (dr.Read())
                {
                    str = dr.GetString(1);
                }
                dr.Close();

                comm = new SqlCommand("SELECT CURRENT_USER", conn);
                var user = comm.ExecuteScalar();

                ViewBag.Message = $"DONE READING....{str}....{user}";
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.ToString();
            }
            // ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach(var h in Request.Headers.AllKeys)
            {
                sb.AppendLine($"{h}={Request.Headers[h]}");
            }
            ViewBag.Message = sb.ToString();

            return View();
        }
    }
}