using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;

namespace DurableFun
{
    public static class Function1
    {
        static int x = 0;
        static List<string>  outputs = new List<string>();
        static HttpClient httpClient = new HttpClient();
        [FunctionName("Function1")]
        public static async Task<string> RunOrchestrator(
            [OrchestrationTrigger] DurableOrchestrationContext context)
        {

            var caseSwitch = "1";
            while (caseSwitch != "4")
            {
                caseSwitch = await context.CallActivityAsync<string>("case_print", caseSwitch);
            }
            return caseSwitch;

            //DateTime deadline = context.CurrentUtcDateTime.Add(TimeSpan.FromSeconds(10));
            //await context.CreateTimer(deadline, CancellationToken.None);

            // return await context.CallActivityAsync<string>("LargeString", "http://bing.com");

            // var x = await context.CallSubOrchestratorAsync<string>("SayHello", null);

            //return x + "main";

            // //int i = 0;
            // //outputs.Add($"A: x={x}(i={i}) - {context.IsReplaying}");

            // ////while (i < 3 && x < 3)
            // //for(int y=0;y<3;y++)
            // //{
            // //    outputs.Add($"B: x={x}(i={i};y={y}) - {context.IsReplaying}");
            // //    var status = await context.CallActivityAsync<string>("CallHTTP", "http://bing.com");
            // //    outputs.Add($"C: x={x}(i={i};y={y}) - {context.IsReplaying}");
            // //    if (status == "DONE")
            // //    {
            // //        break;
            // //    }
            // //    i++;
            // //    x++;
            // //}
            // //return outputs;


            // //List<string> outputs = new List<string>();

            // //if (!context.IsReplaying)
            // //{
            // //    outputs = new List<string>();
            // //}
            // i++ ;

            //// outputs.Add($"A: {i} - {context.IsReplaying}");
            // while (i<10)
            // {
            //     outputs.Add($"B: {i} - {context.IsReplaying}");

            //     outputs.Add(await context.CallActivityAsync<string>("CallHTTP", $"RETRY - {i} - {context.IsReplaying}"));
            //     //   outputs.Add($"C: {i} - {context.IsReplaying}");

            //     if (outputs.Count > 16)
            //     {
            //         return outputs;
            //     }
            // }
            // //return outputs;



            // outputs.Add($"A: {i} - {context.IsReplaying}");

            // // Replace "hello" with the name of your Durable Activity Function.
            // outputs.Add(await context.CallActivityAsync<string>("Function1_Hello", $"Tokyo {context.IsReplaying}"));

            // outputs.Add($"B: {i} - {context.IsReplaying}");
            // DateTime deadline = context.CurrentUtcDateTime.Add(TimeSpan.FromMinutes(1));
            // await context.CreateTimer(deadline, CancellationToken.None);

            // outputs.Add($"C: {i} - {context.IsReplaying}");
            // outputs.Add(await context.CallActivityAsync<string>("Function1_Hello", $"Seattle {context.IsReplaying}"));

            // outputs.Add($"D: {i} - {context.IsReplaying}");
            // deadline = context.CurrentUtcDateTime.Add(TimeSpan.FromMinutes(1));
            // await context.CreateTimer(deadline, CancellationToken.None);

            // outputs.Add($"E: {i} - {context.IsReplaying}");
            // outputs.Add(await context.CallActivityAsync<string>("Function1_Hello", $"London {context.IsReplaying}"));

            // outputs.Add($"F: {i} - {context.IsReplaying}");
            // deadline = context.CurrentUtcDateTime.Add(TimeSpan.FromMinutes(1));
            // await context.CreateTimer(deadline, CancellationToken.None);

            // outputs.Add($"G: {i} - {context.IsReplaying}");
            // // returns ["Hello Tokyo!", "Hello Seattle!", "Hello London!"]
            // //return outputs;
        }

        [FunctionName("case_print")]
        static public string caswswtichf([ActivityTrigger] string caseSwitch, ILogger log)
        {


            switch (caseSwitch)
            {
                case "1":
                    log.LogInformation($"Case 1");
                    //prefom some logic first and then move to next case
                    caseSwitch = "2";
                    break;
                case "2":
                    log.LogInformation($"Case 2");
                    //prefom some logic first and then move to next case
                    caseSwitch = "3";
                    break;
                case "3":
                    log.LogInformation($"Case 3");
                    //prefom some logic first and then move to next case
                    caseSwitch = "4";
                    break;
                default:
                    log.LogInformation($"Default case");
                    caseSwitch = "1";
                    break;
            }

            return caseSwitch;
        }

        [FunctionName("Function2")]
        public static async Task<string> RunOrchestrator2(
            [OrchestrationTrigger] DurableOrchestrationContext context)
        {
            DateTime deadline = context.CurrentUtcDateTime.Add(TimeSpan.FromSeconds(10));
            await context.CreateTimer(deadline, CancellationToken.None);

           // return await context.CallActivityAsync<string>("LargeString", "http://bing.com");

            return await context.CallActivityAsync<string>("Function1_Hello", "Function2");

        }

        [FunctionName("Function1_Hello")]
        public static string SayHello([ActivityTrigger] string name, ILogger log)
        {
            log.LogInformation($"Saying hello to {name}.");
            return $"Hello {name}!";
        }

        [FunctionName("CallHTTP")]
        public static async Task<string> CallHTTP([ActivityTrigger] string name, ILogger log)
        {
            log.LogInformation($"Saying hello to {name}.");
            
            var str = await httpClient.GetStringAsync("http://bing.com");
            return $"Hello {name}! - {str.Length}";
        }

        [FunctionName("LargeString")]
        public static async Task<string> LargeString([ActivityTrigger] string name, ILogger log)
        {
            log.LogInformation($"from LargeString Saying hello to {name}.");

            string xyz = new string('a',  512 * 1024);

            return xyz;
        }

        [FunctionName("Function1_HttpStart")]
        public static async Task<HttpResponseMessage> HttpStart(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post")]HttpRequestMessage req,
            [OrchestrationClient]DurableOrchestrationClient starter,
            ILogger log)
        {
            // Function input comes from the request content.
            string instanceId = await starter.StartNewAsync("Function1", null);

            log.LogInformation($"Started orchestration with ID = '{instanceId}'.");

            return starter.CreateCheckStatusResponse(req, instanceId);
        }

        /*
         *  "output": [
        "A: 1 - False",
        "A: 2 - True",
        "Hello Tokyo False!",
        "B: 2 - False",
        "A: 3 - True",
        "Hello Tokyo False!",
        "B: 3 - True",
        "C: 3 - False",
        "A: 4 - True",
        "Hello Tokyo False!",
        "B: 4 - True",
        "C: 4 - True",
        "Hello Seattle False!",
        "D: 4 - False",
        "A: 5 - True",
        "Hello Tokyo False!",
        "B: 5 - True",
        "C: 5 - True",
        "Hello Seattle False!",
        "D: 5 - True",
        "E: 5 - False",
        "A: 6 - True",
        "Hello Tokyo False!",
        "B: 6 - True",
        "C: 6 - True",
        "Hello Seattle False!",
        "D: 6 - True",
        "E: 6 - True",
        "Hello London False!",
        "F: 6 - False",
        "A: 7 - True",
        "Hello Tokyo False!",
        "B: 7 - True",
        "C: 7 - True",
        "Hello Seattle False!",
        "D: 7 - True",
        "E: 7 - True",
        "Hello London False!",
        "F: 7 - True",
        "G: 7 - False"
    ],
         */
    }
}