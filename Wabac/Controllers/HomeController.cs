﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Wabac.Models;

namespace Wabac.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            //var arrID = Request.Headers["X-ARR-LOG-ID"];
            //var appRequestID = Guid.NewGuid();
            //appLog.Log($"Link ARR ID with applicsation Request ID {arrID} - {appRequestID}");

            //StringBuilder sb = new StringBuilder();
            //foreach(var x in Request.Headers.AllKeys)
            //{
            //    sb.Append($"{x}:");
            //}
            //ViewBag.MyHeader = sb.ToString();
            //Response.AppendHeader("PPRATAP", "WABAC");

            //ViewBag.MyHeader = "X-ARR-LOG-ID :" + Request.Headers["X-ARR-LOG-ID"];

            //Response.AppendHeader("MY-X-ARR-LOG-ID", Request.Headers["X-ARR-LOG-ID"]);

            return View();
        }

        private static string GetLinkerTime(Assembly assembly, TimeZoneInfo target = null)
        {
            var filePath = assembly.Location;
            const int PeHeaderOffset = 60;
            const int LinkerTimestampOffset = 8;

            

            var buffer = new byte[2048];

            using (var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                stream.Read(buffer, 0, 2048);
            }

            var offset = BitConverter.ToInt32(buffer, PeHeaderOffset);
            var secondsSince1970 = BitConverter.ToInt32(buffer, offset + LinkerTimestampOffset);
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

            var linkTimeUtc = epoch.AddSeconds(secondsSince1970);

            var tz = target ?? TimeZoneInfo.Local;
            //var localTime = TimeZoneInfo.ConvertTimeFromUtc(linkTimeUtc, tz);

            TimeZoneInfo est;
            est = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");

            var localTime = TimeZoneInfo.ConvertTime(linkTimeUtc, est);

            return filePath + " -- " + assembly.FullName + " -- " + localTime.ToString("F");
        }


        public ActionResult About()
        {
            var buildDate = GetLinkerTime(Assembly.GetExecutingAssembly());
            ViewBag.Message = "Hi, I was built on from bitbucket " + buildDate;

            

            return View();
        }

        public ActionResult Test404(int id)
        {
            

            if (id ==0)throw new HttpException(404, "from my website");

            if(id==1)return new HttpStatusCodeResult(404, "from my website");

           if(id==2)return new HttpNotFoundResult("from my website");

            //if (id == 3) return new FileNotFoundResult()
            //{
            //    Message = "No Dinner "
            //};
 

            JsonResult res = new JsonResult();
            res.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            List<string> regions = new List<string>() { "CHINA", "EMEA", "INDIA", "LATAM", "North America" };
            List<int> values = new List<int>() { 1, 10, 20, 5, 7 };
            res.Data = new { Regions = regions, Values = values };
            Response.TrySkipIisCustomErrors = true;
            Response.StatusCode = 404;
            return res;
        }

        public ActionResult Contact()
        {
            ViewBag.Message = $"Your contact page. User.Identity.Name={User.Identity.Name} {User.Identity}";

            return View();
        }

        public ActionResult TestMe()
        {
            ViewBag.Email = User.Identity.Name; // "ppratap@microsoft.com";
            TestModel m = new TestModel();
            return View(m);
        }
    }
}