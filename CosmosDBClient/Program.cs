﻿using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Newtonsoft.Json;
using System;

namespace CosmosDBClient
{
    class Program
    {
        private const string EndpointUrl = "https://jamescase.documents.azure.com:443/";
        private const string PrimaryKey = "l3TytwN6ybiTokiamW1U605X4d4rUUTcL5ATcT505OCSro2iWQhBBsREOie7oUnKYPmmzZNbOpLFRHt7KZVZTg==";
        private const string DatabaseName = "TestDB";
        private const string CollectionName = "TestCollection";
        private static DocumentClient client;

        static void Main(string[] args)
        {
            client = new DocumentClient(new Uri(EndpointUrl), PrimaryKey);

            var t = client.CreateDatabaseIfNotExistsAsync(new Database { Id = CollectionName });
            t.Wait();

         
            var t1 = client.CreateDocumentCollectionIfNotExistsAsync(UriFactory.CreateDatabaseUri(DatabaseName), new DocumentCollection { Id = CollectionName });
            t1.Wait();

            for (int i = 0; i < 100; i++)
            {
                TestObject obj = new TestObject() { ID = Guid.NewGuid().ToString() };

                var t2 = client.CreateDocumentAsync(UriFactory.CreateDocumentCollectionUri(DatabaseName, CollectionName), obj);
                t2.Wait();

                Console.WriteLine(i);

                System.Threading.Thread.Sleep(60 * 1000);
            }
            
        }
    }

    public class TestObject
    {
        [JsonProperty(PropertyName = "ID")]
        public string ID { get; set; }
        public string ObjectName { get; set; }
       
    }
}
