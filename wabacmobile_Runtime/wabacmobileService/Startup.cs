using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(wabacmobileService.Startup))]

namespace wabacmobileService
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureMobileApp(app);
        }
    }
}