﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace XMLCode
{
    class Program
    {
        static void Main(string[] args)
        {
            string xmlContent = System.IO.File.ReadAllText(@"D:\Temp\CaseData\03012019\XML Code\justxml.txt");
            var doc = new XmlDocument();
            doc.LoadXml(xmlContent);

            var root = doc.DocumentElement;
            var bodyNode = root.ChildNodes[1];
            var CustomerByElementsResponse_sync = bodyNode.ChildNodes[0];

            int topLevelCount =  0;
            int subLevelCount = 0;

            var returnList = new List<Customer>();
            foreach (XmlNode node in CustomerByElementsResponse_sync.ChildNodes)
            {
                string pRC = null, empId = null, eId = null;

                if (node.SelectSingleNode("DirectResponsibility") == null) continue;

                topLevelCount++;

                XmlNodeList drs;
                drs = node.SelectNodes("DirectResponsibility");
                foreach (XmlNode dr in drs)
                {
                    if (node.SelectSingleNode("ExternalID") != null)
                    {
                        eId = node["ExternalID"].InnerText;
                    }

                    subLevelCount++;

                    XmlNode directResponsibility = dr;
                    pRC = directResponsibility["PartyRoleCode"].InnerText;
                    empId = directResponsibility["EmployeeID"].InnerText;

                    returnList.Add(new Customer
                    {
                        ExternalId = eId,
                        PartyRoleCode = pRC,
                        EmployeeId = empId
                    });
                }
            }
        }
    }

    public class SoapMessage
    {
        public string Uri { get; set; }
        public string ContentXml { get; set; }
        public string SoapAction { get; set; }
    }

    public class Customer
    {
        public string ExternalId { get; set; }
        public string PartyRoleCode { get; set; }
        public string EmployeeId { get; set; }
    }
}
