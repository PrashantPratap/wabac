﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Emails
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                if (args.Count() != 4)
                {
                    Console.WriteLine("please provide email address, password, port and subject as shown below");
                    Console.WriteLine("Emails.exe ppratap@microsoft.com password 25 \"from staging website\"");
                    return;
                }
                String userName = args[0];
                String password = args[1];
                String subject = args[3];
                int port = 25;
                if( !int.TryParse(args[2], out port) )
                {
                    Console.WriteLine($"failed to get port number from {args[2]}");
                    return;
                }

                MailMessage msg = new MailMessage();
                msg.To.Add(new MailAddress(userName));
                msg.From = new MailAddress(userName);
                msg.Subject = subject;
                msg.Body = subject;
                msg.IsBodyHtml = true;
                SmtpClient client = new SmtpClient();
                client.Host = "smtp.office365.com";
                client.Credentials = new System.Net.NetworkCredential(userName, password);
                client.Port = port;
                client.EnableSsl = true;
                client.Send(msg);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
