﻿using Microsoft.Azure.ServiceBus;
using System;
using System.Text;
using System.Threading.Tasks;

namespace AddEvents2ServiceBus
{
    
    class Program
    {
        static string ServiceBusConnectionString = "Endpoint=sb://starbucks.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=tgBRnsQXO3dxHcNhwQC6zY9LKqIfm+1/ai5dT2aARmQ=";
        static string TopicName = "topicone";
        static ITopicClient topicClient;
        public static async Task Main(string[] args)
        {
            Console.WriteLine("AddEvents2ServiceBus...adding messages");
            int numberOfMessages = 10000;

            var str = Environment.GetEnvironmentVariable("DOTNET_SEND_NUMBER_MESSAGES");
            if(!int.TryParse(str, out numberOfMessages))
            {
                Console.WriteLine("AddEvents2ServiceBus...no env for number of messages, default to 10,000");
                numberOfMessages = 10000;
            }

            str = Environment.GetEnvironmentVariable("DOTNET_SERVICEBUS_STR");
            if(string.IsNullOrEmpty(str))
            {
                Console.WriteLine("AddEvents2ServiceBus...no env for connection string, default to ppratap");
            }
            else
            {
                ServiceBusConnectionString = str;
            }

            str = Environment.GetEnvironmentVariable("DOTNET_TOPICNAME");
            if (string.IsNullOrEmpty(str))
            {
                Console.WriteLine("AddEvents2ServiceBus...no env for topic, default to ppratap");
            }
            else
            {
                TopicName = str;
            }

            topicClient = new TopicClient(ServiceBusConnectionString, TopicName);
            //await SendMessagesAsync(numberOfMessages);

            for(int i=0;i<25;i++)
            {

            }

            await topicClient.CloseAsync();
            Console.WriteLine("AddEvents2ServiceBus...done");
        }

        static async Task SendMessagesAsync(int numberOfMessagesToSend)
        {
            try
            {
                for (var i = 0; i < numberOfMessagesToSend; i++)
                {
                    string messageBody = $"Message {i}";
                    var message = new Message(Encoding.UTF8.GetBytes(messageBody));
                    await topicClient.SendAsync(message);
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine($"{DateTime.Now} :: Exception: {exception.Message}");
            }
        }
    }
}
