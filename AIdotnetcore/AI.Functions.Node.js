//const appInsights = require("applicationinsights");
//appInsights.setup();
//const client = appInsights.defaultClient;

let appInsights = require("applicationinsights");
//appInsights.setup("d126b530-d879-4e7c-9477-06af17c9b00d").start();
//let client = appInsights.defaultClient;

if (!appInsights.defaultClient) {
   appInsights.setup("d126b530-d879-4e7c-9477-06af17c9b00d").start();
}
let client = appInsights.defaultClient;
// var client = appInsights.getClient();

module.exports = function (context, req) {
    context.log('JavaScript HTTP trigger function processed a request.');



client.trackEvent({name: "1 my custom event", properties: {customProperty: "custom property value"}});
client.trackException({exception: new Error("1 handled exceptions can be logged with this method")});
client.trackMetric({name: "1 custom metric", value: 3});
client.trackTrace({message: "1 trace message"});
client.trackDependency({target:"http://dbname", name:"1 select customers proc", data:"SELECT * FROM Customers", duration:231, resultCode:0, success: true, dependencyTypeName: "ZSQL"});
client.trackRequest({name:"1 GET /customers", url:"http://myserver/customers", duration:309, resultCode:200, success:true});
 
 context.log('ai done.2');

    if (req.query.name || (req.body && req.body.name)) {
        context.res = {
            // status: 200, /* Defaults to 200 */
            body: "Hello " + (req.query.name || req.body.name)
        };
    }
    else {
        context.res = {
            status: 400,
            body: "Please pass a name on the query string or in the request body"
        };
    }
    context.done();
};