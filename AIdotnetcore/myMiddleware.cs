﻿using Microsoft.ApplicationInsights.DataContracts;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AIdotnetcore
{
    public class myMiddleware
    {
        private readonly RequestDelegate _mynext;

        public myMiddleware(RequestDelegate next)
        {
            _mynext = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            var telemetry = new Microsoft.ApplicationInsights.TelemetryClient();
            var strGuid = Guid.NewGuid().ToString();

            telemetry.TrackTrace("Request",
                           SeverityLevel.Verbose,
                           new Dictionary<string, string> { { "request", httpContext.Request.Path }, {"RequestID", strGuid } });

            await _mynext.Invoke(httpContext);

            telemetry.TrackTrace("Response",
                   SeverityLevel.Verbose,
                   new Dictionary<string, string> { { "response", httpContext.Request.Path }, { "RequestID", strGuid } });
        }
    }
}
