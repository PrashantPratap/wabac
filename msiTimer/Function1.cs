using System;
using System.Net.Http;
using System.Net.Http.Headers;
using Microsoft.Azure.Services.AppAuthentication;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;

namespace msiTimer
{
    public static class Function1
    {
        [FunctionName("Function12")]
        public static void Run([TimerTrigger("0 */5 * * * *")]TimerInfo myTimer, ILogger log)
        {
            log.LogInformation($"C# Timer trigger function executed at: {DateTime.Now}");

            //var azureServiceTokenProvider = new AzureServiceTokenProvider();
            //var accessTokenTask = azureServiceTokenProvider.GetAccessTokenAsync("https://msione.azurewebsites.net/");

            //accessTokenTask.Wait();
            //var accessToken = accessTokenTask.Result;

            //var url = "https://msione.azurewebsites.net/";
            //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            //var responseTask = client.GetStringAsync(url);
            //responseTask.Wait();
            //var response = responseTask.Result;


            var azureServiceTokenProvider = new AzureServiceTokenProvider();
            var accessTokenTask = azureServiceTokenProvider.GetAccessTokenAsync("https://graph.windows.net/");

            accessTokenTask.Wait();
            var accessToken = accessTokenTask.Result;

            var url = "https://graph.windows.net/me?api-version=1.6";
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            var responseTask = client.GetStringAsync(url);
            responseTask.Wait();
            var response = responseTask.Result;
            log.LogInformation($"GRAPH API (ME) : {response}");

        }

        static HttpClient client = new HttpClient();
    }
}
