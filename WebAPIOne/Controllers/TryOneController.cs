﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebAPIOne.Controllers
{
    public class TryOneController : ApiController
    {
        // GET: api/TryOne
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/TryOne/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/TryOne
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/TryOne/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/TryOne/5
        public void Delete(int id)
        {
        }
    }
}
