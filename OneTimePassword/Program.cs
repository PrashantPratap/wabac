﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace OneTimePassword
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            for(int i=0;i<10;i++)
            {
                Console.WriteLine($"**** *** GetCode = {GetCode()}");
                Console.WriteLine($"Date = {DateTime.UtcNow}");
                System.Threading.Thread.Sleep(30 * 1000);
            }
            
        }

static string secret = "62E4799F-BEA6-48C1-87E9-AB07EE19E2BF";
static int digits = 6;
static public int GetCode()
{
    var hmac = new HMACSHA512(
        ASCIIEncoding.ASCII.GetBytes(secret));
    var counterBytes = BitConverter.GetBytes(Counter);
    if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(counterBytes, 0, 8);
            }
    var hash = hmac.ComputeHash(counterBytes);
    int offset = hash[hash.Length - 1] & 0x0F;
    var truncatedHash = new byte[]
        {
            (byte)(hash[offset + 0] & 0x7F),
            hash[offset + 1],
            hash[offset + 2],
            hash[offset + 3]
        };
    if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(truncatedHash, 0, 4);
            }
    var number = BitConverter.ToInt32(truncatedHash, 0);
    return number % DigitsDivisor[digits];
}

static public bool ValidateCode(int code)
{
    int newCode = GetCode();
    if (newCode == code)
    {
        return true;
    }
    return false;
}
private static readonly int[] DigitsDivisor = new int[] 
{
    0, 0, 0, 0,
    10000, 100000,
    1000000, 10000000,
    100000000, 1000000000
};


static private readonly DateTime Epoch = 
            new DateTime(1970, 1, 1, 0, 0, 0,
                DateTimeKind.Utc);
static private long Counter
{
    get
    {
        var currTime = DateTime.UtcNow;
        var seconds = (currTime.Ticks - Epoch.Ticks)
                    / 10000000;
        return (seconds / 120);
    }
}
    }
}
