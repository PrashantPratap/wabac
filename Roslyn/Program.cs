using Microsoft.Build.Locator;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.FindSymbols;
using Microsoft.CodeAnalysis.MSBuild;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roslyn
{
    class Program
    {
        static void Main(string[] args)
        {
            MSBuildLocator.RegisterDefaults();
            var workspace = MSBuildWorkspace.Create();

            var solutionTask = workspace.OpenSolutionAsync(@"D:\Projects\John\PwC.SDK\PwC.SDK\PwC.SDK.sln");
            solutionTask.Wait();
            var solution = solutionTask.Result;

            var project = solution.Projects.SingleOrDefault(p => p.Name == "PwC.SDK.Web");

            var compilationTask = project.GetCompilationAsync();
            compilationTask.Wait();
            var compilation = compilationTask.Result;

            //to search for functions within our solution
            var myFunction = compilation.GetSymbolsWithName(s => s.StartsWith("TileMetrics"), SymbolFilter.All).FirstOrDefault();

            //to search for all reference of this above function without solution
            var callerTask2 = SymbolFinder.FindReferencesAsync(myFunction, solution);

            // to search for function within our solution and all referenced dlls
            var externalFunctionsTask = SymbolFinder.FindDeclarationsAsync(project, "MapRoute", true);
            externalFunctionsTask.Wait();
            var externalFunctions = externalFunctionsTask.Result;

            //SourceLocation

            foreach (var externalFunction in externalFunctions)
            {
                var callerTask = SymbolFinder.FindReferencesAsync(externalFunction, solution);
                callerTask.Wait();
                var callers = callerTask.Result;

                Console.WriteLine($"Function name is {externalFunction.ToDisplayString()}");
                foreach (var referenced in callers)
                {
                    Console.WriteLine($"Number of references {referenced.Definition.Name} {referenced.Locations.Count()}");
                    foreach (var location in referenced.Locations)
                    {
                        Console.WriteLine($"FileName (line#) {location.Location.SourceTree.FilePath} ({location.Location.GetLineSpan().StartLinePosition.Line + 1}) ({location.Location.GetLineSpan().EndLinePosition.Line + 1})");
                    }
                }
            }
        }
    }
}
