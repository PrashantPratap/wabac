﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace OTPTest
{
    class Program
    {
        static int z = 5;
        static void Main(string[] args)
        {
            int x = 0, y = 0;
           
                for(int i=0;i<240;i++)
                {
                    x = GetCode();
                    System.Threading.Thread.Sleep(1000 * (z-1));
                    Console.WriteLine($"{i} : {x} :  {ValidateCode(x)}");
                }
            
            //Console.WriteLine("Hello World!");
        }

        static string secret = "AEA53AE6-249F-4A10-BC12-DB2D5EE151A8";
        static int digits = 6;
        static public int GetCode(bool bCheckOld = false)
        {
            var hmac = new HMACSHA512(
                ASCIIEncoding.ASCII.GetBytes(secret));
            var counterBytes = BitConverter.GetBytes(Counter(bCheckOld));
            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(counterBytes, 0, 8);
            }
            var hash = hmac.ComputeHash(counterBytes);
            int offset = hash[hash.Length - 1] & 0x0F;
            var truncatedHash = new byte[]
                {
            (byte)(hash[offset + 0] & 0x7F),
            hash[offset + 1],
            hash[offset + 2],
            hash[offset + 3]
                };
            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(truncatedHash, 0, 4);
            }
            var number = BitConverter.ToInt32(truncatedHash, 0);
            return number % DigitsDivisor[digits];
        }

        static public bool ValidateCode(int code)
        {
            int newCode = GetCode();
            if (newCode == code)
            {
                return true;
            }
            Console.WriteLine($": {code} : false : {newCode}");
            newCode = GetCode(true);
            if (newCode == code)
            {
                Console.WriteLine($": {code} : true : {newCode}");
                return true;
            }
            Console.WriteLine($": {code} : 2false : {newCode}");
            return false;
        }
        private static readonly int[] DigitsDivisor = new int[]
        {
    0, 0, 0, 0,
    10000, 100000,
    1000000, 10000000,
    100000000, 1000000000
        };


        static private readonly DateTime Epoch =
                    new DateTime(1970, 1, 1, 0, 0, 0,
                        DateTimeKind.Utc);
        static private long Counter(bool bCheckOld = false)
        {

            var currTime = DateTime.UtcNow;
            var seconds = (currTime.Ticks - Epoch.Ticks)
                        / 10000000;

            if (bCheckOld)
            {
                seconds = (seconds - z);
            }

            return (seconds / z);

        }
    }
}
