﻿using Microsoft.Azure.WebJobs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebJobLoggingTest
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Threading.Thread t = new System.Threading.Thread(WorkerThread);
            t.Start();

            JobHostConfiguration config = new JobHostConfiguration();
            config.StorageConnectionString= "DefaultEndpointsProtocol=https;AccountName=wabacstorage;AccountKey=wXrZ0Xnqc+YflIo3hRFMEh+uJQG20TnP3L8OkGK6Hl6zkFI7uO/Qp++8avz6dlV0Go6Vl3P+7owHxupYnBBRog==";
            //config.DashboardConnectionString = "DefaultEndpointsProtocol=https;AccountName=wabacstorage;AccountKey=wXrZ0Xnqc+YflIo3hRFMEh+uJQG20TnP3L8OkGK6Hl6zkFI7uO/Qp++8avz6dlV0Go6Vl3P+7owHxupYnBBRog==";
            config.DashboardConnectionString = null;

            JobHost host = new JobHost(config);
            host.RunAndBlock();

        }

        static void WorkerThread()
        {
            while(true)
            {
                Console.WriteLine($"from Wabac WebJobs testing logging....{DateTime.Now.ToString("o")}");
                System.Threading.Thread.Sleep(60 * 1000); // (60 * 1000);
            }
        }
    }
}
