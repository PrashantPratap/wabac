﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeyVaultExtension.Models
{
    public enum enumControlType
    {
        Text,
        Area,
        Boolean,
        DropDownSingleSelection,
        DropDownMultipleSelection,
        CascadeTopLevel,
        CascadeSubLevel,
        CascadeURL
    }
    public class OmegaModel
    {
        public string Label { get; set; }
        public enumControlType ControlType { get; set; }
        public string DataOptions { get; set; }
    }
    
    [HtmlTargetElement("omega")]
    public class OmegaTagHelper : TagHelper
    {
        [HtmlAttributeName("Name")]
        public string Name { get; set; }
        [HtmlAttributeName("label")]
        public string Label { get; set; }

        [HtmlAttributeName("control-type")]
        public enumControlType ControlType { get; set; }

        [HtmlAttributeName("data-options")]
        public string DataOptions { get; set; }

        public override void Process(
            TagHelperContext context,
            TagHelperOutput output)
        {
            output.TagName = "details";
            output.TagMode = TagMode.StartTagAndEndTag;

            var sb = new StringBuilder();
            switch(this.ControlType)
            {
                case enumControlType.Text:
                    {
                        sb.Append($"<li>{this.Label} <input id='{this.Name}' type='text' /></li>");
                        break;
                    }
                case enumControlType.Area:
                    {
                        sb.Append($"<li>{this.Label} <textarea  id='{this.Name}' rows='4' cols='50' /></textarea>");
                        break;
                    }
                case enumControlType.Boolean:
                    {
                        sb.Append($"<li>{this.Label} <input id='{this.Name}' /></li>");
                        break;
                    }
            }
            output.PreContent.SetHtmlContent(sb.ToString());
        }
    }
}


/*
https://tahirnaushad.com/2017/08/27/asp-net-core-2-0-mvc-custom-tag-helpers/
https://demos.telerik.com/kendo-ui/switch/index

 <div class="demo-section">
        <div class="settings-head"></div>
        <ul>
            <li>Notifications <input type="checkbox" id="notifications-switch" aria-label="Notifications Switch" checked="checked" /></li>
            <li>Send notifications <input id="mail-switch" aria-label="Mail Switch" /></li>
            <li>Always visible <input id="visible-switch" aria-label="Visible Switch" /></li>
            <li>Display real name <input id="name-switch" aria-label="Name Switch" /></li>
        </ul>
    </div>

    <style>
        .demo-section ul {
            margin: 0;
            padding: 0;
        }

            .demo-section ul li {
                margin: 0;
                padding: 10px 10px 10px 20px;
                min-height: 28px;
                line-height: 28px;
                vertical-align: middle;
                border-top: 1px solid rgba(128,128,128,.5);
            }

        .demo-section {
            min-width: 220px;
            margin-top: 50px;
            padding: 0;
        }

            .demo-section ul li .k-switch {
                float: right;
            }

        .settings-head {
            height: 66px;
            background: url('../content/web/switch/settings-title.png') no-repeat 20px 50% #2db245;
        }
    </style>
 
 
 */
