﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RedisSessionMVC.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            var now = DateTime.UtcNow;
            var s = Session["myTime"];
            if(s == null)
            {
                s = now;
                Session["myTime"] = s;
            }
            var m = Environment.GetEnvironmentVariable("ComputerName");

            ViewBag.Message = $"Machine name {m}<br />Current time {now}.<br />Session time {s}";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}