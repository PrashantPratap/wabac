﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RedisSession
{
    public partial class About : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            object obj = Session["one"];
            if(obj == null)
            {
                Session["one"] = "First One---";
                Label1.Text = "First One---";
            }
            else
            {
                Label1.Text = obj.ToString() + DateTime.Now.ToString();
            }

            System.Threading.Thread.Sleep(110 * 1000);
        }
    }
}