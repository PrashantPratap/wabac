﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BasCase.Models;
using System.Net.Http;
using System.Threading;
using Microsoft.Extensions.Logging;

namespace BasCase.Controllers
{
    public class HomeController : Controller
    {
        private static HttpClient client = new HttpClient();
        private static AutoResetEvent ase = new AutoResetEvent(false);

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        private readonly ILogger _logger;
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult LongRunning(int id)
        {
            ase.WaitOne(id);
            return new JsonResult("OK");
        }

        public async Task<IActionResult> Test(int id, int id2)
        {
            try
            {
                _logger.LogCritical("Test sleep time {id} ; cancel time {id2}", id, id2);

                HttpRequestMessage request = new HttpRequestMessage();
                request.RequestUri = new Uri($"http://{Request.Host}/Home/LongRunning/{id}");

                var cancellationToken = new CancellationTokenSource(TimeSpan.FromMilliseconds(id2)).Token;
                var response = await client.SendAsync(request, cancellationToken);
            }
            catch(Exception ex)
            {

            }

            return new JsonResult("OK");
        }

        public async Task<IActionResult> TestEx(int id, int id2)
        {
            try
            {
                _logger.LogCritical("Test sleep time {id} ; cancel time {id2}", id, id2);

                HttpRequestMessage request = new HttpRequestMessage();
                request.RequestUri = new Uri($"http://coreservice.azurewebsites.net/Home/LongRunning/{id}");

                var cancellationToken = new CancellationTokenSource(TimeSpan.FromMilliseconds(id2)).Token;
                var response = await client.SendAsync(request, cancellationToken);
            }
            catch (Exception ex)
            {

            }

            return new JsonResult("OK");
        }
    }
}
