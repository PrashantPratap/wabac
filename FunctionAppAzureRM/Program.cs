﻿using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System;
using System.Net.Http;
using System.Net.Http.Headers;

namespace FunctionAppAzureRM
{
    
    class Program
    {
        static string tenantId = "72f988bf-86f1-41af-91ab-2d7cd011db47";
        static string username = "http://wabacuser";
        static string password = "Micr0s0ft";
        static string token = string.Empty;
        static string subscription = "25d360ff-1859-4a27-aada-a216484284b3";
        static string resourceGroup = "highcpu";
        static string appServicePlan = "highcpu";

        static HttpClient httpClient = new HttpClient();
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            GetToken();

            var appServicePlan = "https://management.azure.com/subscriptions/25d360ff-1859-4a27-aada-a216484284b3/resourceGroups/highcpu/providers/Microsoft.Web/serverfarms/highcpu";

            //var appServicePlan = $"https://management.azure.com/subscriptions/{subscription}/resourceGroups/{resourceGroup}/providers/Microsoft.Web/serverfarms/{appServicePlan}";
            //var str = Get(appServicePlan + "?api-version=2016-09-01");


            //new
            //{
            //    location = Location,
            //    Sku = new
            //    {
            //        Name = "F1"
            //    }
            //}

            //{"location":"helo","Sku":{"Name":"F1"}}


            Put(appServicePlan + "?api-version=2016-09-01", "{ \"location\":\"South Central US\",\"Sku\":{\"Name\":\"S2\"}}");

            Get(appServicePlan + "/SKU" + "?api-version=2016-09-01");
           // var str1 = Post("https://management.azure.com/subscriptions/25d360ff-1859-4a27-aada-a216484284b3/resourceGroups/highcpu/providers/Microsoft.Web/serverfarms/highcpu/restartSites?api-version=2016-09-01", string.Empty);
        }

        public static void GetToken()
        {
            string url = "https://login.windows.net/" + tenantId;
            var authContext = new AuthenticationContext(url);
            var credential = new ClientCredential(username, password);
            var t = authContext.AcquireTokenAsync("https://management.azure.com/", credential);
            t.Wait();

            if (t.Result == null)
            {
                throw new InvalidOperationException("Failed to obtain the JWT token");
            }

            token = t.Result.AccessToken;
        }

        static string Get(string url)
        {
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var t = httpClient.GetStringAsync(url);
            t.Wait();
            return t.Result;
        }

        static string Put(string url, string jsonData)
        {
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

            StringContent postData = new StringContent(jsonData, System.Text.Encoding.UTF8, "application/json");
            

            var t = httpClient.PutAsync(url, postData);
            t.Wait();
            var t1 = t.Result.Content.ReadAsStringAsync();
            return t1.Result;
        }

        static string Post(string url, string jsonData)
        {
            StringContent postData = new StringContent(jsonData, System.Text.Encoding.UTF8, "application/json");

            var t = httpClient.PostAsync(url, postData);
            t.Wait();
            var t1 = t.Result.Content.ReadAsStringAsync();
            return t1.Result;
        }
    }
}
