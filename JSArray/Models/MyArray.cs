﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JSArray.Models
{
    public class MyArray
    {
        public string one { get; set; }
        public string two { get; set; }
        public string three { get; set; }
    }

    public class MyArray2
    {
        public string four { get; set; }
        public string five { get; set; }
        public string six { get; set; }
    }

    public class WholePost
    {
        public List<MyArray> a { get; set; }
        public List<MyArray2> b { get; set; }
        public string AsmId { get; set; }
        public string AsmName { get; set; }

    }
}
