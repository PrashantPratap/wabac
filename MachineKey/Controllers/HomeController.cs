﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MachineKey.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            HttpCookie protectCookie = Request.Cookies["PROTECT"];

            if (protectCookie == null)
            {
                var b = System.Web.Security.MachineKey.Protect(System.Text.Encoding.ASCII.GetBytes("hello"), "hello");
                var s = System.Text.Encoding.ASCII.GetString(System.Web.Security.MachineKey.Unprotect(b, "hello"));

                protectCookie = new HttpCookie("PROTECT");
                protectCookie.Value = Convert.ToBase64String(b);
                Response.Cookies.Add(protectCookie);

                ViewBag.Message = $"Proctected TXT {Convert.ToBase64String(b)} : Unprotected TXT {s} : ProcessID is {System.Diagnostics.Process.GetCurrentProcess().Id} : Machine name is {Environment.MachineName}";
            }
            else
            {
                var s = System.Text.Encoding.ASCII.GetString(System.Web.Security.MachineKey.Unprotect(Convert.FromBase64String(protectCookie.Value), "hello"));

                ViewBag.Message = $"Unprotected TXT {s} : ProcessID is {System.Diagnostics.Process.GetCurrentProcess().Id} : Machine name is {Environment.MachineName}";
            }

            


            


            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}