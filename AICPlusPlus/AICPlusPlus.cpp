// AICPlusPlus.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"



void jsonPageView(char* json, int max, char* url, char* pageName, int milliseconds)
{
	char strtime[200];
	
	const char* fmt = "%Y-%m-%dT%H:%M:%S.000Z";

	struct tm newtime;
	__int64 ltime;
	_time64(&ltime);
	_gmtime64_s(&newtime, &ltime);
	strftime(strtime, sizeof(strtime), fmt, &newtime);


	// duration
	long hours = milliseconds / (1000 * 60 * 60);
	long lessThanhour = milliseconds - (hours * 1000 * 60 * 60);
	long minutes = lessThanhour / (1000 * 60);
	long lessThanMin = lessThanhour - (minutes * 1000 * 60);
	long seconds = lessThanMin / 1000;
	long ms = lessThanMin - (seconds * 1000);

	char duration[40];
	sprintf_s(duration,40, "%02ld:%02ld:%02ld.%03ld",
		hours, minutes, seconds, ms);

	json[0] = '\0';


	// json
	strcat_s(json, max, "[{\"time\": \"");
	strcat_s(json, max, strtime);
	strcat_s(json, max, "\",");

	strcat_s(json, max, "\"iKey\": \"552701b7-b288-42af-b2b2-76bf7c82cfac\",");
	strcat_s(json, max, "\"name\": \"Microsoft.ApplicationInsights.552701b7-b288-42af-b2b2-76bf7c82cfac.Pageview\",");
	strcat_s(json, max, "\"data\": { \"baseType\" : \"PageviewData\", \"baseData\": { \"ver\": 2, \"name\": \"");
	strcat_s(json, max, pageName);
	strcat_s(json, max, "\", \"url\": \"");
	strcat_s(json, max, url);
	strcat_s(json, max, "\", \"duraion\": \"");
	strcat_s(json, max, duration);
	strcat_s(json, max, "\"}}}]");

}


void jsonEvent(char* json, int max, char* eventName)
{
	char strtime[200];

	const char* fmt = "%Y-%m-%dT%H:%M:%S.000Z";

	struct tm newtime;
	__int64 ltime;
	_time64(&ltime);
	_gmtime64_s(&newtime, &ltime);
	strftime(strtime, sizeof(strtime), fmt, &newtime);

	json[0] = '\0';


	// json
	strcat_s(json, max, "[{\"time\": \"");
	strcat_s(json, max, strtime);
	strcat_s(json, max, "\",");

	strcat_s(json, max, "\"iKey\": \"552701b7-b288-42af-b2b2-76bf7c82cfac\",");
	strcat_s(json, max, "\"name\": \"Microsoft.ApplicationInsights.552701b7-b288-42af-b2b2-76bf7c82cfac.Pageview\",");
	strcat_s(json, max, "\"data\": { \"baseType\" : \"EventData\", \"baseData\": { \"ver\": 2, \"name\": \"");
	strcat_s(json, max, eventName);
	strcat_s(json, max, "\"}}}]");

}

void jsonTraceMsg(char* json, int max, char* msg)
{
	char strtime[200];

	const char* fmt = "%Y-%m-%dT%H:%M:%S.000Z";

	struct tm newtime;
	__int64 ltime;
	_time64(&ltime);
	_gmtime64_s(&newtime, &ltime);
	strftime(strtime, sizeof(strtime), fmt, &newtime);

	json[0] = '\0';


	// json
	strcat_s(json, max, "[{\"time\": \"");
	strcat_s(json, max, strtime);
	strcat_s(json, max, "\",");

	strcat_s(json, max, "\"iKey\": \"552701b7-b288-42af-b2b2-76bf7c82cfac\",");
	strcat_s(json, max, "\"name\": \"Microsoft.ApplicationInsights.552701b7-b288-42af-b2b2-76bf7c82cfac.Message\",");
	strcat_s(json, max, "\"data\": { \"baseType\" : \"MessageData\", \"baseData\": { \"ver\": 2, \"message\": \"");
	strcat_s(json, max, msg);
	strcat_s(json, max, "\"}}}]");

}

void jsonDependency(char* json, int max, char* name, bool success, char* resultCode, long milliseconds)
{
	char strtime[200];

	const char* fmt = "%Y-%m-%dT%H:%M:%S.000Z";

	struct tm newtime;
	__int64 ltime;
	_time64(&ltime);
	_gmtime64_s(&newtime, &ltime);
	strftime(strtime, sizeof(strtime), fmt, &newtime);
	
	// duration
	long hours = milliseconds / (1000 * 60 * 60);
	long lessThanhour = milliseconds - (hours * 1000 * 60 * 60);
	long minutes = lessThanhour / (1000 * 60);
	long lessThanMin = lessThanhour - (minutes * 1000 * 60);
	long seconds = lessThanMin / 1000;
	long ms = lessThanMin - (seconds * 1000);

	char duration[40];
	sprintf_s(duration, 40, "%02ld:%02ld:%02ld.%03ld",
		hours, minutes, seconds, ms);

	json[0] = '\0';

	char id[40];
	sprintf_s(id, 40, "%ld", GetTickCount64());


	// json
	strcat_s(json, max, "[{\"time\": \"");
	strcat_s(json, max, strtime);
	strcat_s(json, max, "\",");

	strcat_s(json, max, "\"iKey\": \"552701b7-b288-42af-b2b2-76bf7c82cfac\",");
	strcat_s(json, max, "\"name\": \"Microsoft.ApplicationInsights.552701b7-b288-42af-b2b2-76bf7c82cfac.RemoteDependency\",");
	strcat_s(json, max, "\"data\": { \"baseType\" : \"RemoteDependencyData\", \"baseData\": { \"ver\": 2, \"name\": \"");
	strcat_s(json, max, name);
	strcat_s(json, max, "\", \"id\": \"");
	strcat_s(json, max, id);
	strcat_s(json, max, "\", \"resultcode\": \"");
	strcat_s(json, max, resultCode);
	strcat_s(json, max, "\", \"success\": ");
	if(success)strcat_s(json, max, "true");else strcat_s(json, max, "false");
	strcat_s(json, max, ", \"duration\": \"");
	strcat_s(json, max, duration);
	strcat_s(json, max, "\"}}}]");

}

HINTERNET hSession;
void Send2AI(char* aiData)
{
	hSession = WinHttpOpen(L"My AI Client", WINHTTP_ACCESS_TYPE_DEFAULT_PROXY,
		WINHTTP_NO_PROXY_NAME, WINHTTP_NO_PROXY_BYPASS, 0);

	HANDLE hConnect = nullptr;
	HANDLE hRequest = nullptr;
	BOOL result = false;

	hConnect = WinHttpConnect(hSession, L"dc.services.visualstudio.com", INTERNET_DEFAULT_HTTPS_PORT, 0);
	hRequest = WinHttpOpenRequest(hConnect, L"POST", L"/v2/track", nullptr, WINHTTP_NO_REFERER, WINHTTP_DEFAULT_ACCEPT_TYPES, WINHTTP_FLAG_SECURE);
	result = WinHttpSendRequest(hRequest, NULL, (DWORD)-1, aiData, strlen(aiData), strlen(aiData), 0);

	WinHttpCloseHandle(hSession);
}

int main()
{
	


	char aiData[1024];
	jsonPageView(aiData, 1024, "myURL 303", "mypage 202", 2998);
	Send2AI(aiData);
	jsonEvent(aiData, 1024, "My event 404");
	Send2AI(aiData);
	jsonTraceMsg(aiData, 1024, "My trace msg 505");
	Send2AI(aiData);
	jsonDependency(aiData, 1024, "my remote call 606", true, "101", 9876);
	Send2AI(aiData);
    
	

	return 0;
}




