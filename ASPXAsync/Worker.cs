﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASPXAsync
{
    public class Worker
    {
        public static List<string> ourLogs = new List<string>();
        public static void Log(string msg)
        {
            ourLogs.Add(msg);
        }

        public static void ClearLogs()
        {
            ourLogs.Clear();
        }

        public static void workerFunction(int x)
        {
            try
            {
                Log($"Starting worker function {DateTime.UtcNow}, sleep time {x} ms");
                System.Threading.Thread.Sleep(x);
                Log($"End worker function {DateTime.UtcNow}, sleep time {x} ms");
            }
            catch(Exception ex)
            {
                // Log to Application Insights
            }
        }
    }
}