﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ASPXAsync
{
    public partial class _Default : Page
    {
        
       
        protected void Page_Load(object sender, EventArgs e)
        {
            Worker.Log($"Starting default page at {DateTime.UtcNow}");
            Task.Run(() => { Worker.workerFunction(120 * 1000); }); // 120 seconds
            Task.Run(() => { Worker.workerFunction(60 * 1000); }); // 60 seconds
            Task.Run(() => { Worker.workerFunction(30 * 1000); }); // 30 seconds
            Task.Run(() => { Worker.workerFunction(3 * 1000); }); // 3 seconds
            Worker.Log($"End default page at {DateTime.UtcNow}");
        }
    }
}