﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Mvc;

namespace PInvokeWeb.Controllers
{
    public class HomeController : Controller
    {
        [DllImport("PInvokeC.dll")] 
        private static extern int fnPInvokeC();

        [DllImport("PInvokeC.dll")]
        private static extern int MyAdd(int a, int b);
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            string path = Environment.GetEnvironmentVariable("PATH");
            try
            {
                 int x = fnPInvokeC();
                //int x = MyAdd(1,4);
                ViewBag.Message = $"Your application description page. {x} {path}";
            }
            catch (Exception ex)
            {
                ViewBag.Message = $"{ex.ToString()} {path}";
            }

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}