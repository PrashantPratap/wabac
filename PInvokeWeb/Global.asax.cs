﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace PInvokeWeb
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //string path = Environment.GetEnvironmentVariable("PATH");
            //string binDir = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Bin");
            //Environment.SetEnvironmentVariable("PATH", path + ";" + binDir);
        }
    }
}
