﻿

    using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventHub
{
    class Program
    {
        static void Main(string[] args)
        {
            var connectionStringFapp =
            "Endpoint=sb://wabaceventhub.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=dZ5CHN8y9DUtCmztPHOzONWUSe5wnOwQijTRKymiOLk=";
            
            var connectionStringBuilderFapp = new ServiceBusConnectionStringBuilder(connectionStringFapp);
            connectionStringBuilderFapp.EntityPath = "eventhubone";
            

            var eventHubClientFapp = EventHubClient.CreateFromConnectionString(connectionStringBuilderFapp.ToString());

            for (int i = 870; i < 880; i++)
            {
                string strMsg = $"error ({i}+)";
                var data = Encoding.ASCII.GetBytes(strMsg);
                //eventHubClientFapp.Send(new EventData(data));

                strMsg = $"timeout ({i}+)";
                 data = Encoding.ASCII.GetBytes(strMsg);
                //eventHubClientFapp.Send(new EventData(data));

                strMsg = $"nlog ({i}+)";
                data = Encoding.ASCII.GetBytes(strMsg);
                eventHubClientFapp.Send(new EventData(data));

                //System.Threading.Thread.Sleep(60 * 1000);
            }
            eventHubClientFapp.Close();

            return;

            // send 

            //var connectionString =
            //"Endpoint=sb://wabaceventhuba.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=D51MVdUu5Xvjb5bqqlOpf2NNGtKJx5mUfIwsmmvkVT4=";

            //var connectionStringBuilder = new ServiceBusConnectionStringBuilder(connectionString);
            //connectionStringBuilder.EntityPath = "insights-logs-applicationgatewayaccesslog";

            //var eventHubClient = EventHubClient.CreateFromConnectionString(connectionStringBuilder.ToString());

            //for (int i = 0; i < 10; i++)
            //{
            //    string strMsg = $"From console to Application Gate A {i} @ " + DateTime.Now.ToString("hh:mm:ss.FFF");
            //    var data = Encoding.ASCII.GetBytes(strMsg);
            //    eventHubClient.Send(new EventData(data));
            //}
            //eventHubClient.Close();

            //connectionStringBuilder.EntityPath = "wabaceventhubb";

            //eventHubClient = EventHubClient.CreateFromConnectionString(connectionStringBuilder.ToString());

            //for (int i = 0; i < 10; i++)
            //{
            //    string strMsg = $"From console to Application Gate B {i} @ " + DateTime.Now.ToString("hh:mm:ss.FFF");
            //    var data = Encoding.ASCII.GetBytes(strMsg);
            //    eventHubClient.Send(new EventData(data));
            //}
            //eventHubClient.Close();

            //connectionString = "Endpoint=sb://wabacsb.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=al4r+aRc3ci067LZspl9adJkWdnaN8eu+40J2DRpeXs=";
            //connectionStringBuilder = new ServiceBusConnectionStringBuilder(connectionString);
            //connectionStringBuilder.EntityPath = "firsteventhubfromwabacsb";

            //eventHubClient = EventHubClient.CreateFromConnectionString(connectionStringBuilder.ToString());

            //for (int i = 0; i < 10; i++)
            //{
            //    string strMsg = $"From console to firsteventhubfromwabacsb {i} @ " + DateTime.Now.ToString("hh:mm:ss.FFF");
            //    var data = Encoding.ASCII.GetBytes(strMsg);
            //    eventHubClient.Send(new EventData(data));
            //}


            //string eventHubName = "insights-logs - applicationgatewayaccesslog
            //EventProcessorHost eventProcessorHost = new EventProcessorHost(eventProcessorHostName, eventHubName, EventHubConsumerGroup.DefaultGroupName, eventHubConnectionString, storageConnectionString);
            //Console.WriteLine("Registering EventProcessor...");
            //var options = new EventProcessorOptions();
            //options.ExceptionReceived += (sender, e) => { Console.WriteLine(e.Exception); };
            //eventProcessorHost.RegisterEventProcessorAsync<SimpleEventProcessor>(options).Wait();

            //Console.WriteLine("Receiving. Press enter key to stop worker.");
            //Console.ReadLine();
            //eventProcessorHost.UnregisterEventProcessorAsync().Wait();



            // customer service bus
            var connectionString = "Endpoint=sb://dma-mavlink-appgw.servicebus.windows.net/;SharedAccessKeyName=AppGWPutEvent;SharedAccessKey=+1mXVj1pSh6BPlt87XtkBVUG1wIHjwH5U19oTsG0uOQ=";
            //var connectionString = "Endpoint=sb://dme-mavlink-appgw.servicebus.windows.net/;SharedAccessKeyName=AppGWPutEvent;SharedAccessKey=jOlP/PhVyFA8ItkOdfL7vFKPYeIkCGamDwmncQr/YfA=";
            var connectionStringBuilder = new ServiceBusConnectionStringBuilder(connectionString);
            connectionStringBuilder.EntityPath = "insights-logs-applicationgatewayaccesslog";

            var eventHubClient = EventHubClient.CreateFromConnectionString(connectionStringBuilder.ToString());

            EventHubConsumerGroup group = eventHubClient.GetDefaultConsumerGroup();
            var receiver = group.CreateReceiver(eventHubClient.GetRuntimeInformation().PartitionIds[0]); //0,1,2,3
            bool receive = true;
            string myOffset;
            while (receive)
            {
                var message = receiver.Receive();
                if (message == null)
                {
                    Console.WriteLine(String.Format("Received message offset: {0} \nbody: {1}", 0, "NULL"));
                    continue;
                }
                myOffset = message.Offset;
                string body = Encoding.UTF8.GetString(message.GetBytes());

                try
                {
                    var r = Newtonsoft.Json.JsonConvert.DeserializeObject<RootObject>(body);
                    Console.WriteLine(String.Format("Received message offset: {0} \nbody: {1}", myOffset, r.records[0].properties.requestUri));
                }
                catch (Exception ex)
                {
                    Console.WriteLine(String.Format("Received message offset: {0} \nbody: {1}", myOffset, body));
                }

            }




        }
    }

    class SimpleEventProcessor : IEventProcessor
    {
        Stopwatch checkpointStopWatch;

        async Task IEventProcessor.CloseAsync(PartitionContext context, CloseReason reason)
        {
            Console.WriteLine("Processor Shutting Down. Partition '{0}', Reason: '{1}'.", context.Lease.PartitionId, reason);
            if (reason == CloseReason.Shutdown)
            {
                await context.CheckpointAsync();
            }
        }

        Task IEventProcessor.OpenAsync(PartitionContext context)
        {
            Console.WriteLine("SimpleEventProcessor initialized.  Partition: '{0}', Offset: '{1}'", context.Lease.PartitionId, context.Lease.Offset);
            this.checkpointStopWatch = new Stopwatch();
            this.checkpointStopWatch.Start();
            return Task.FromResult<object>(null);
        }

        async Task IEventProcessor.ProcessEventsAsync(PartitionContext context, IEnumerable<EventData> messages)
        {
            foreach (EventData eventData in messages)
            {
                string data = Encoding.UTF8.GetString(eventData.GetBytes());

                //Console.WriteLine(string.Format("Message received.  Partition: '{0}', Data: '{1}'",
                //    context.Lease.PartitionId, data));

                try
                {
                    var r = Newtonsoft.Json.JsonConvert.DeserializeObject<RootObject>(data);
                    Console.WriteLine(String.Format("Received message PartitionId: {0} \nbody: {1}", context.Lease.PartitionId, r.records[0].properties.requestUri));
                }
                catch (Exception ex)
                {
                    Console.WriteLine(String.Format("Received message PartitionId: {0} \nbody: {1}", context.Lease.PartitionId, data));
                }
            }

            //Call checkpoint every 5 minutes, so that worker can resume processing from 5 minutes back if it restarts.
            if (this.checkpointStopWatch.Elapsed > TimeSpan.FromMinutes(5))
            {
                await context.CheckpointAsync();
                this.checkpointStopWatch.Restart();
            }
        }
    }

    public class Properties
    {
        public string instanceId { get; set; }
        public string clientIP { get; set; }
        public int clientPort { get; set; }
        public string httpMethod { get; set; }
        public string requestUri { get; set; }
        public string requestQuery { get; set; }
        public string userAgent { get; set; }
        public int httpStatus { get; set; }
        public string httpVersion { get; set; }
        public int receivedBytes { get; set; }
        public int sentBytes { get; set; }
        public int timeTaken { get; set; }
        public string sslEnabled { get; set; }
    }

    public class Record
    {
        public string resourceId { get; set; }
        public string operationName { get; set; }
        public string time { get; set; }
        public string category { get; set; }
        public Properties properties { get; set; }
    }

    public class RootObject
    {
        public List<Record> records { get; set; }
    }


}
