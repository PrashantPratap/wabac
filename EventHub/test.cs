﻿using System;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Luis;
using Microsoft.Bot.Builder.Luis.Models;
using Microsoft.Bot.Connector;
using System.Threading;

[Serializable]
[LuisModel("54201f7e-4b88-4686-841c-5f47364eb875", "737bf507949b4bada390bf914b41176d")]
public class TemperatureLuisDialog : LuisDialog<object>
{

    // Name of  entity
    public const string Entity_TemperatureUnitTo = "TemperatureUnitTo"; // fahrenheit";
    public const string Entity_TemperatureUnitFrom = "TemperatureUnitFrom"; //celsius";
    public const string Entity_TemperatureValue = "TemperatureValue"; //100";

    // methods to handle LUIS intents

    [LuisIntent("")]
    public async Task None(IDialogContext context, LuisResult result)
    {
        string message = $"try 100 f to c";
        await context.PostAsync(message);
        context.Wait(MessageReceived);
    }

    [LuisIntent("Convert")]
    public async Task SerialNumber(IDialogContext context, LuisResult result)
    {
        string tempTo = "not found";
        string tempFrom = "not found";
        string tempValue = "not found";

        EntityRecommendation entity;
        if (result.TryFindEntity(Entity_TemperatureUnitTo, out entity))
        {
            tempTo = entity.Entity;
        }
        if (result.TryFindEntity(Entity_TemperatureUnitFrom, out entity))
        {
            tempFrom = entity.Entity;
        }
        if (result.TryFindEntity(Entity_TemperatureValue, out entity))
        {
            tempValue = entity.Entity;
        }
        /*
        [°C] = ([°F] - 32) × 5/9
        [°F] = [°C] × 9/5 + 32
        */
        double d = 0.0, t = 0.0;

        if (double.TryParse(tempValue, out d))
        {
            if (tempTo.Contains('f') || tempTo.Contains('F'))
            {
                t = ((d * 9) / 5) + 32;
            }
            else
            {
                t = ((d - 32) * 5) / 9;
            }
        }
        string answer = String.Format("{0:0.00}", t);

        //string message = $"{answer}";
        string message = $"answer={answer} ; tempTo={tempTo} ; tempFrom={tempFrom} ; tempValue={tempValue}";
        await context.PostAsync(message);

        context.Wait(this.MessageReceived);
    }
}