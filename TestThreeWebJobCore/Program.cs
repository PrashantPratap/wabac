﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;

namespace TestThreeWebJobCore
{
    class Program
    {
        //static void Main(string[] args)
        //{
        //    Console.WriteLine("Hello World!");
        //}

        static async System.Threading.Tasks.Task Main(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            var serviceCollection = new ServiceCollection()
                .ProjectSpecificDataAccess(configuration.GetConnectionString("ApplicationDataContext"));

            var provider = serviceCollection.BuildServiceProvider();

            var jobHostConfiguration = new JobHostConfiguration(configuration);
            jobHostConfiguration.Queues.VisibilityTimeout = TimeSpan.FromSeconds(15);
            jobHostConfiguration.Queues.MaxDequeueCount = 3;
            jobHostConfiguration.LoggerFactory = new LoggerFactory().AddConsole();
            jobHostConfiguration.JobActivator = new MicrosoftDependencyInjectionActivator(provider);

            if (jobHostConfiguration.IsDevelopment)
            {
                jobHostConfiguration.UseDevelopmentSettings();
            }

            var jobHost = new JobHost(jobHostConfiguration);

            jobHost.RunAndBlock();
        }
    }
}
