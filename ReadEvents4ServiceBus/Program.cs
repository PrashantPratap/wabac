﻿using Microsoft.ApplicationInsights;
using Microsoft.Azure.ServiceBus;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ReadEvents4ServiceBus
{
    class Program
    {
        static string ServiceBusConnectionString = "Endpoint=sb://starbucks.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=tgBRnsQXO3dxHcNhwQC6zY9LKqIfm+1/ai5dT2aARmQ=";
        static string TopicName = "topicone";
        static string SubscriptionName = "topicsub";
        static ISubscriptionClient subscriptionClient;
        static TelemetryClient aiClient;

        static int count = 0;

        public static async Task Main(string[] args)
        {
            
            Console.WriteLine("ReadEvents4ServcieBus...reading msg");
            int numberOfMessages = 64;

            aiClient = new TelemetryClient();

            var str = Environment.GetEnvironmentVariable("DOTNET_AI_KEY");
            if (string.IsNullOrEmpty(str))
            {
                Console.WriteLine("ReadEvents4ServcieBus...no env for AI key (DOTNET_AI_KEY) , default to ppratap");
                aiClient.InstrumentationKey = "0475fe7b-d678-4174-8e44-d1f81d48b525";
            }
            else
            {
                aiClient.InstrumentationKey = str;
            }

            str = Environment.GetEnvironmentVariable("DOTNET_MAX_CONCURRENT");
            if (!int.TryParse(str, out numberOfMessages))
            {
                Console.WriteLine("ReadEvents4ServcieBus...no env for number of concurrent msg (DOTNET_MAX_CONCURRENT), default to 64");
                numberOfMessages = 64;
            }

            str = Environment.GetEnvironmentVariable("DOTNET_SERVICEBUS_STR");
            if (string.IsNullOrEmpty(str))
            {
                Console.WriteLine("ReadEvents4ServcieBus...no env for connection string (DOTNET_SERVICEBUS_STR), default to ppratap");
            }
            else
            {
                ServiceBusConnectionString = str;
            }

            str = Environment.GetEnvironmentVariable("DOTNET_TOPICNAME");
            if (string.IsNullOrEmpty(str))
            {
                Console.WriteLine("ReadEvents4ServcieBus...no env for topic (DOTNET_TOPICNAME), default to ppratap");
            }
            else
            {
                TopicName = str;
            }

            str = Environment.GetEnvironmentVariable("DOTNET_TOPICSUB");
            if (string.IsNullOrEmpty(str))
            {
                Console.WriteLine("ReadEvents4ServcieBus...no env for  sub (DOTNET_TOPICSUB), default to ppratap");
            }
            else
            {
                SubscriptionName = str;
            }

            subscriptionClient = new SubscriptionClient(ServiceBusConnectionString, TopicName, SubscriptionName);

            var messageHandlerOptions = new MessageHandlerOptions(ExceptionReceivedHandler)
            {
                MaxConcurrentCalls = numberOfMessages,
                AutoComplete = false
            };

            // Register the function that will process messages
            subscriptionClient.RegisterMessageHandler(ProcessMessagesAsync, messageHandlerOptions);

            Console.WriteLine("reading...");
            Console.ReadKey();
        }

        static async Task ProcessMessagesAsync(Message message, CancellationToken token)
        {
            count++;
            await subscriptionClient.CompleteAsync(message.SystemProperties.LockToken);
            aiClient.TrackTrace("DOTNET_MSG_READ");

            if(count%100 == 0)
            {
                Console.WriteLine($"Msg read = {count}");
            }
        }

        static Task ExceptionReceivedHandler(ExceptionReceivedEventArgs exceptionReceivedEventArgs)
        {
            Console.WriteLine($"Message handler encountered an exception {exceptionReceivedEventArgs.Exception}.");
            var context = exceptionReceivedEventArgs.ExceptionReceivedContext;
            Console.WriteLine("Exception context for troubleshooting:");
            Console.WriteLine($"- Endpoint: {context.Endpoint}");
            Console.WriteLine($"- Entity Path: {context.EntityPath}");
            Console.WriteLine($"- Executing Action: {context.Action}");
            return Task.CompletedTask;
        }
    }
}
