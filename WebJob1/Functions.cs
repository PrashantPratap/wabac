﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;

namespace WebJob1
{
    public class Functions
    {
        // This function will get triggered/executed when a new message is written 
        // on an Azure Queue called queue.
        public static void ProcessQueueMessage([QueueTrigger("queue")] string message, TextWriter log)
        {
            log.WriteLine(message);
        }

        [NoAutomaticTrigger]
        public static void NoAuotmaticTriggerFunction(TextWriter logger)
        {
            logger.WriteLine($"NoAuotmaticTriggerFunction: {DateTime.Now} ");
        }

        [NoAutomaticTrigger]
        public static void ThisOneSleep(TextWriter logger)
        {
            int pid = System.Diagnostics.Process.GetCurrentProcess().Id;
            logger.WriteLine($"ThisOneSleep: before sleep {pid}:{DateTime.Now} ");
            for (int i = 0; i < 180; i++)
            {
                System.Threading.Thread.Sleep(1000 );
                logger.WriteLine($"ThisOneSleep: after sleep {pid}:{DateTime.Now} ");
            }
            logger.WriteLine($"ThisOneSleep: end {pid}:{DateTime.Now} ");
        }
    }
}
