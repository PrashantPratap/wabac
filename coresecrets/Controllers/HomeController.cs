﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using coresecrets.Models;
using Microsoft.Extensions.Configuration;

namespace coresecrets.Controllers
{
    public class HomeController : Controller
    {
        // Place to store the Config object and use in this controller
        private readonly IConfiguration Config;

        // Constructor that that takes IConfiguration is called on instantiation thanks to Dependency injection
        public HomeController(IConfiguration config)
        {
            Config = config;
        }

        public IActionResult Index()
        {
            ViewData["Parent:ChildOne"] = Config["Parent:ChildOne"];
            ViewData["Parent:ChildTwo"] = Config["Parent:ChildTwo"];
            ViewData["ConnectionStrings:Parent:ChildTwo"] = Config["ConnectionStrings:Parent:ChildTwo"];
            ViewData["CUSTOMCONNSTR_Parent:ChildTwo"] = Config.GetConnectionString("Parent:ChildTwo");
            
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
