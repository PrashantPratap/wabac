﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SQLAD.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About(int id)
        {
            try
            {
                SqlConnection conn = null;
                string ConnectionString = string.Empty;


                string useerName = HttpContext.User.Identity.Name;

                if (id ==1)
                {
                    ConnectionString = @"Data Source=adtestwabac.database.windows.net; Authentication=Active Directory Integrated; Initial Catalog=adtestdb;";
                    conn = new SqlConnection(ConnectionString);
                    conn.Open();
                }
                else
                {
                    ConnectionString = @"Data Source=adtestwabac.database.windows.net; Initial Catalog=adtestdb;";
                    conn = new SqlConnection(ConnectionString);
                    conn.AccessToken = Request.Headers["X-MS-TOKEN-AAD-ID-TOKEN"];
                    
                    conn.Open();
                }

                SqlCommand comm = new SqlCommand("Select * from Persons", conn);
                var dr = comm.ExecuteReader();

                string str = string.Empty;
                while(dr.Read())
                {
                    str = dr.GetString(1);
                }
                dr.Close();

                //comm = new SqlCommand("SELECT CURRENT_USER", conn);
                //var user = comm.ExecuteScalar();

                //ViewBag.Message = $"DONE READING....{str}....{user}";
            }
            catch(Exception ex)
            {
                ViewBag.Message = ex.ToString();
            }
           // ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            //ViewBag.Message = "Your contact page." + Request.Headers["X-MS-TOKEN-AAD-ID-TOKEN"];

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (var h in Request.Headers.AllKeys)
            {
                sb.AppendLine($"{h}={Request.Headers[h]}");
            }
            ViewBag.Message = sb.ToString();

            return View();
        }
    }
}