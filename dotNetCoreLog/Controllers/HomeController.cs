﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using dotNetCoreLog.Models;
using Microsoft.Extensions.Logging;

namespace dotNetCoreLog.Controllers
{
public class HomeController : Controller
{
    private readonly ILogger _logger;

    public HomeController(ILogger<HomeController> logger)
    {
        _logger = logger;
    }

    public IActionResult Index()
    {
        return View();
    }

    public IActionResult About()
    {
        _logger.LogDebug("DEBUG hello from about");
        _logger.LogTrace("TRACE hello from about");
        _logger.LogInformation("INFO hello from about");
        _logger.LogWarning("WARNING hello from about");
        _logger.LogError("ERROR hello from about");
        _logger.LogCritical("CRITICAL hello from about");
        ViewData["Message"] = "Your application description page.";

        return View();
    }

    public IActionResult Contact()
    {
        ViewData["Message"] = "Your contact page.";

        return View();
    }

    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
}
