﻿using Microsoft.ApplicationInsights;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dotNetCoreLog
{
public static class AILoggerFactoryExtensions
{
    public static ILoggingBuilder AddAILogging(this ILoggingBuilder builder)
    {
        builder.Services.AddSingleton<ILoggerProvider, AILoggerProvider>();
        return builder;
    }
}
public class AILoggerProvider : ILoggerProvider
{
    static AILogger _AILogger = new AILogger(System.Environment.GetEnvironmentVariable("APPINSIGHTS_INSTRUMENTATIONKEY"));
    public ILogger CreateLogger(string categoryName)
    {
        return _AILogger;
    }
    public void Dispose()
    {
    }
}
    
public class AILogger : ILogger
{
    TelemetryClient _aiClient = new TelemetryClient();
    public AILogger(string aiKey)
    {
        _aiClient.InstrumentationKey = aiKey;
    }
    public IDisposable BeginScope<TState>(TState state)
    {
        return null;
    }
    public bool IsEnabled(LogLevel logLevel)
    {
        return true;
    }
    public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
    {
        _aiClient.TrackTrace(formatter(state, exception));
    }
}
}
