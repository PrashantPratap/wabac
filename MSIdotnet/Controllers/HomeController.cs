﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.Services.AppAuthentication;
using MSIdotnet.Models;

namespace MSIdotnet.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page." + User.Identity.Name;

            return View();
        }
        static HttpClient client = new HttpClient();
        public IActionResult Contact()
        {
            var azureServiceTokenProvider = new AzureServiceTokenProvider();
            var accessTokenTask = azureServiceTokenProvider.GetAccessTokenAsync("https://wabaconead.azurewebsites.net/");//("86f27f56-4ac0-4916-b9e0-a9d097aba83b");

            accessTokenTask.Wait();
            var accessToken = accessTokenTask.Result;


            var url = "https://wabaconead.azurewebsites.net/home/about";
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            var responseTask = client.GetStringAsync(url);

            responseTask.Wait();
            var reponse = responseTask.Result;

            
            ViewData["Message"] = "Your contact page." + accessToken + " HTML Response " + reponse;

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
