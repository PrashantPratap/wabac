﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Security;

namespace CheckSSL
{
    class Program
    {
        static void Main(string[] args)
        {
            var handler = new HttpClientHandler();

            handler.ServerCertificateCustomValidationCallback =
                    (httpRequestMessage, cert, cetChain, policyErrors) =>
                    {
                        if (cert.Subject.Contains("azurewebsites.net"))
                        {
                            return false;
                        }
                        return true;
                    };

            HttpClient httpClient = new HttpClient(handler);
            var t = httpClient.GetStringAsync("https://sixshot.org");
            t.Wait();
            var str = t.Result;


            Console.WriteLine("Hello World!");
        }

    }
}
