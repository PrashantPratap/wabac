﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Timers;

namespace CustomTriggerWebJob
{
    public class Functions
    {
        public static void ProcessQueueMessage([TimerTrigger(typeof(CustomScheduleDaily))] TimerInfo timerInfo, TextWriter log)
        {
            log.WriteLine($"WebJob trigger ....{DateTime.UtcNow}.....{timerInfo.ToString()} ");
        }
    }
    //https://blogs.msdn.microsoft.com/amitagarwal/2018/01/01/custom-schedule-for-azure-web-job-timer-triggers/
    public class CustomScheduleDaily : DailySchedule
    {
        public CustomScheduleDaily() : base("16:20:00", "16:30:00")
        {
        }
    }
}
