﻿using ClosedXML.Excel;
using OfficeOpenXml;
using OfficeOpenXml.FormulaParsing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyExcel
{
    class Program
    {
        static void Main(string[] args)
        {
            //CreateNewfile();
            //Readfile();

            //CreateNewCSVfile(); //doesnt work
            //ReadCSVfile(); //doesnt work

        //    ReadfileClosedXML();

            ReadCSVfileClosedXML(); //doesnt work
        }


        static void ReadCSVfileClosedXML()
        {
            string fileName = @"d:\temp\myexcel.csv";
            var workbook = new XLWorkbook(fileName);
            var worksheet = workbook.Worksheet(1);

            for (int iRow = 1; iRow < 6; iRow++) 
            {
                var row = worksheet.Row(iRow);

                for (int iCol = 1; iCol < 6; iCol++)
                {
                    var col = row.Cell(iCol);
                    Console.WriteLine(col.GetString());
                }
            }
        }

        static void ReadfileClosedXML()
        {
            string fileName = @"d:\temp\myexcel.xlsx";
            var workbook = new XLWorkbook(fileName);
            var worksheet = workbook.Worksheet(1);

            for (int iRow = 1; iRow < 6; iRow++)
            {
                var row = worksheet.Row(iRow);

                for (int iCol = 1; iCol < 6; iCol++)
                {
                    var col = row.Cell(iCol);
                    Console.WriteLine(col.GetString());
                }
            }
        }
        static void ReadCSVfile()
        {
            FileInfo existingFile = new FileInfo(@"d:\temp\myexcel.csv");
            using (ExcelPackage xlPackage = new ExcelPackage(existingFile))
            {
                ExcelWorksheet worksheet = xlPackage.Workbook.Worksheets[1];

                for (int iCol = 1; iCol < 6; iCol++)
                    for (int iRow = 1; iRow < 6; iRow++)
                        Console.WriteLine("Cell({0},{1}).Value={2}", iRow, iCol,
                          worksheet.Cells[iRow, iCol].Value);

               
            }
        }
        static void Readfile()
        {
            FileInfo existingFile = new FileInfo(@"d:\temp\myexcel.xlsx");
            using (ExcelPackage xlPackage = new ExcelPackage(existingFile))
            {
                ExcelWorksheet worksheet = xlPackage.Workbook.Worksheets[1];
                
                for (int iCol = 1; iCol < 6; iCol++)
                    for (int iRow = 1; iRow < 6; iRow++)
                    Console.WriteLine("Cell({0},{1}).Value={2}", iRow, iCol,
                      worksheet.Cells[iRow, iCol].Value);
            } 
        }
        static     void CreateNewfile()
        {
            FileInfo newFile = new FileInfo(@"d:\temp\myexcel.xlsx");
            using (ExcelPackage xlPackage = new ExcelPackage(newFile))
            {
                ExcelWorksheet worksheet = xlPackage.Workbook.Worksheets.Add("test sheet");

                worksheet.Cells["A1"].Value = "One";
                worksheet.Cells["B1"].Value = "Two";
                worksheet.Cells["C1"].Value = "Three";
                worksheet.Cells["D1"].Value = "Four";
                worksheet.Cells["E1"].Value = "Five";
                worksheet.Cells["A1:E1"].Style.Font.Bold = true;

                for (char i = 'A'; i < 'F'; i++)
                {
                    for (int j = 2; j < 5; j++)
                    {
                        worksheet.Cells[$"{i}{j}"].Value = $"{i}-{j}";
                    }
                }

                xlPackage.Save();
            }
        }

        static void CreateNewCSVfile()
        {
            FileInfo newFile = new FileInfo(@"d:\temp\myexcel.csv");
            using (ExcelPackage xlPackage = new ExcelPackage(newFile))
            {
                ExcelWorksheet worksheet = xlPackage.Workbook.Worksheets.Add("test sheet");

                worksheet.Cells["A1"].Value = "One";
                worksheet.Cells["B1"].Value = "Two";
                worksheet.Cells["C1"].Value = "Three";
                worksheet.Cells["D1"].Value = "Four";
                worksheet.Cells["E1"].Value = "Five";
                worksheet.Cells["A1:E1"].Style.Font.Bold = true;

                for (char i = 'A'; i < 'F'; i++)
                {
                    for (int j = 2; j < 5; j++)
                    {
                        worksheet.Cells[$"{i}{j}"].Value = $"{i}-{j}";
                    }
                }

                xlPackage.Save();
            }
        }
    }
}
