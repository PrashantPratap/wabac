﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using dotnetContainer2.Models;
using Microsoft.Win32;

namespace dotnetContainer2.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            /*
             * [HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\.NETFramework\v4.0.30319]"SchSendAuxRecord"=dword:00000000
Windows Registry Editor Version 5.00[HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\.NETFramework\v4.0.30319]"SchSendAuxRecord"=dword:00000000
            


            var reg = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\Microsoft\.NETFramework\v4.0.30319", false);
            var x = reg.GetValue("SchSendAuxRecord");

            //HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\XboxLive

            var reg = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\XboxLive", false);
            var x = reg.GetValue("LastActiveUserTime");

     */

            //HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\XboxLive

            var reg = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\wabac", false);
            var x = reg.GetValue("myname");

            ServiceReference1.CrSeedClient obj = new ServiceReference1.CrSeedClient();
            var t = obj.getSeedAsync();

            t.Wait();
            var str = t.Result;

            ViewData["Message"] = "Your contact page." + x;

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
