﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WabacMasterDetails.DB;

namespace WabacMasterDetails.Controllers
{
    public class MasterController : Controller
    {
        private MasterTableContext db = new MasterTableContext();

        // GET: Master
        public async Task<ActionResult> Index()
        {
            return View(await db.MasterTables.ToListAsync());
        }

        // GET: Master/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MasterTable masterTable = await db.MasterTables.FindAsync(id);
            if (masterTable == null)
            {
                return HttpNotFound();
            }
            return View(masterTable);
        }

        // GET: Master/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Master/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,FirstName,LastName,Address")] MasterTable masterTable)
        {
            if (ModelState.IsValid)
            {
                db.MasterTables.Add(masterTable);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(masterTable);
        }

        // GET: Master/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MasterTable masterTable = await db.MasterTables.FindAsync(id);
            if (masterTable == null)
            {
                return HttpNotFound();
            }
            return View(masterTable);
        }

        // POST: Master/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,FirstName,LastName,Address")] MasterTable masterTable)
        {
            if (ModelState.IsValid)
            {
                db.Entry(masterTable).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(masterTable);
        }

        // GET: Master/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MasterTable masterTable = await db.MasterTables.FindAsync(id);
            if (masterTable == null)
            {
                return HttpNotFound();
            }
            return View(masterTable);
        }

        // POST: Master/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            MasterTable masterTable = await db.MasterTables.FindAsync(id);
            db.MasterTables.Remove(masterTable);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
