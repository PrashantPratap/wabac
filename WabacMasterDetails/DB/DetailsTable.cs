namespace WabacMasterDetails.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DetailsTable")]
    public partial class DetailsTable
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public int MasterTableID { get; set; }

        [StringLength(50)]
        public string CarMake { get; set; }

        [StringLength(50)]
        public string CarYear { get; set; }

        [StringLength(50)]
        public string CarColor { get; set; }
    }
}
