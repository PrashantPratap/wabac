namespace WabacMasterDetails.DB
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class MasterTableContext : DbContext
    {
        public MasterTableContext()
            : base("name=MasterTableContext")
        {
        }

        public virtual DbSet<MasterTable> MasterTables { get; set; }
        public virtual DbSet<DetailsTable> DetailsTables { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
