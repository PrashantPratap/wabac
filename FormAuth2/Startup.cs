﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FormAuth2.Startup))]
namespace FormAuth2
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
