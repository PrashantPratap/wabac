﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using LargeFileUpload.Helper;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Net.Http.Headers;
using System.IO;
using System.Text;
using LargeFileUpload.Models;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Globalization;
using LargeFileUpload.ViewModels;
using LargeFileUpload.Attributes;
using Microsoft.AspNetCore.Http.Features;

namespace LargeFileUpload.Controllers
{
    public class Html5Controller : Controller
    {
        public IActionResult Index()
        {
            targetFilePath = Path.GetTempFileName();
            ViewBag.TempFilename = targetFilePath;
            return View();
        }
        static string targetFilePath;

        private static readonly FormOptions _defaultFormOptions = new FormOptions();
        [HttpPost]
        [DisableFormValueModelBinding]
        public async Task<IActionResult> Upload()
        {
            try
            {
                var boundary = MultipartRequestHelper.GetBoundary(
                    MediaTypeHeaderValue.Parse(Request.ContentType),
                    _defaultFormOptions.MultipartBoundaryLengthLimit);

                var reader = new MultipartReader(boundary, HttpContext.Request.Body);

                var section = await reader.ReadNextSectionAsync();

                long start = 0;

                while (section != null)
                {
                    ContentDispositionHeaderValue contentDisposition;
                    var hasContentDispositionHeader = ContentDispositionHeaderValue.TryParse(section.ContentDisposition, out contentDisposition);

                    if (hasContentDispositionHeader)
                    {
                        if (MultipartRequestHelper.HasFileContentDisposition(contentDisposition))
                        {
                            using (var ms = new MemoryStream(10 * 1024 * 1024))
                            {
                                await Request.Body.CopyToAsync(ms);
                                using (var fs = System.IO.File.OpenWrite(targetFilePath))
                                {
                                    fs.Seek(start, SeekOrigin.Begin);
                                    await fs.WriteAsync(ms.ToArray(), 0, (int)ms.Length);
                                }
                            }
                            break;
                        }
                        else if (MultipartRequestHelper.HasFormDataContentDisposition(contentDisposition))
                        {
                            var key = HeaderUtilities.RemoveQuotes(contentDisposition.Name).Value;
                            var encoding = GetEncoding(section);
                            using (var streamReader = new StreamReader(
                                section.Body,
                                encoding,
                                detectEncodingFromByteOrderMarks: true,
                                bufferSize: 1024,
                                leaveOpen: true))
                            {
                                var value = await streamReader.ReadToEndAsync();
                                if (String.Equals(value, "undefined", StringComparison.OrdinalIgnoreCase))
                                {
                                    value = String.Empty;
                                }
                                if (string.Compare(key, "start", true) == 0)
                                {
                                    long.TryParse(value, out start);
                                }
                            }
                        }
                    }
                    section = await reader.ReadNextSectionAsync();
                }
                return Json("OK");
            }
            catch (Exception ex)
            {
                return Json(ex);
            }
        }

        private static Encoding GetEncoding(MultipartSection section)
        {
            MediaTypeHeaderValue mediaType;
            var hasMediaTypeHeader = MediaTypeHeaderValue.TryParse(section.ContentType, out mediaType);
            if (!hasMediaTypeHeader || Encoding.UTF7.Equals(mediaType.Encoding))
            {
                return Encoding.UTF8;
            }
            return mediaType.Encoding;
        }
    }
}