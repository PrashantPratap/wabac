﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LargeFileUpload.Models
{
    public class Profile

    {

        public string Name { get; set; }

        public string AvatarPath { get; set; }

    }
}
