﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace PostCalls.Controllers
{
    public class HomeController : Controller
    {
        static HttpClient client ; 

        static HomeController()
        {
            client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }
        public async Task<ActionResult> Index()
        {
            string resourceAddress = "http://52.161.9.28:3000/";
            string postBody = "{\"req\" : {\"url\" : \"req.url\", \"method\" : \"req.method\"}, \"body\" : \"req.body\"}";
            var res = await client.PostAsync(resourceAddress, new StringContent(postBody, Encoding.UTF8, "application/json"));
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}