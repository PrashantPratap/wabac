﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PostCalls.Controllers
{
    public class TesttwoController : Controller
    {
        // GET: Testtwo/Create
        public ActionResult Two()
        {
            return View();
        }

        // POST: Testtwo/Create
        [HttpPost]
        public ActionResult Two(FormCollection collection)
        {
            
                return View();
           
        }

        // POST: Testtwo/Four
        [HttpPost]
        public ActionResult Four(FormCollection collection)
        {
            ViewBag.Name = collection[1];
            return View();

        }


    }
}
