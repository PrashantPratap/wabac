using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EFdotnetcore.Models;

namespace EFdotnetcore.Controllers
{
    public class NLogsController : Controller
    {
        private readonly wabacDBContext _context;

        public NLogsController(wabacDBContext context)
        {
            _context = context;    
        }

        // GET: NLogs
        public async Task<IActionResult> Index()
        {
            return View(await _context.NLog.ToListAsync());
        }

        // GET: NLogs/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var nLog = await _context.NLog
                .SingleOrDefaultAsync(m => m.Id == id);
            if (nLog == null)
            {
                return NotFound();
            }

            return View(nLog);
        }

        // GET: NLogs/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: NLogs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Application,Logged,Level,Message,UserName,ServerName,Port,Url,Https,ServerAddress,RemoteAddress,Logger,Callsite,Exception")] NLog nLog)
        {
            if (ModelState.IsValid)
            {
                _context.Add(nLog);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(nLog);
        }

        // GET: NLogs/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var nLog = await _context.NLog.SingleOrDefaultAsync(m => m.Id == id);
            if (nLog == null)
            {
                return NotFound();
            }
            return View(nLog);
        }

        // POST: NLogs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Application,Logged,Level,Message,UserName,ServerName,Port,Url,Https,ServerAddress,RemoteAddress,Logger,Callsite,Exception")] NLog nLog)
        {
            if (id != nLog.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(nLog);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!NLogExists(nLog.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(nLog);
        }

        // GET: NLogs/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var nLog = await _context.NLog
                .SingleOrDefaultAsync(m => m.Id == id);
            if (nLog == null)
            {
                return NotFound();
            }

            return View(nLog);
        }

        // POST: NLogs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var nLog = await _context.NLog.SingleOrDefaultAsync(m => m.Id == id);
            _context.NLog.Remove(nLog);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool NLogExists(int id)
        {
            return _context.NLog.Any(e => e.Id == id);
        }
    }
}
