﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using System.Configuration;

namespace EFdotnetcore.Models
{
    public partial class wabacDBContext : DbContext
    {
        public virtual DbSet<NLog> NLog { get; set; }

        // Unable to generate entity type for table 'dbo.bcp'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.LogTable'. Please see the warning messages.

        //        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //        {
        //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
        //            optionsBuilder.UseSqlServer(@"");

        //        }

        public wabacDBContext(DbContextOptions<wabacDBContext> options)
    : base(options)
{ }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<NLog>(entity =>
            {
                entity.ToTable("nLog");

                entity.Property(e => e.Application)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Level)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Logged).HasColumnType("datetime");

                entity.Property(e => e.Logger).HasMaxLength(250);

                entity.Property(e => e.Message).IsRequired();

                entity.Property(e => e.RemoteAddress).HasMaxLength(100);

                entity.Property(e => e.ServerAddress).HasMaxLength(100);

                entity.Property(e => e.UserName).HasMaxLength(250);
            });
        }
    }
}