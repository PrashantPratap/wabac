﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SameCookieTest
{
    class Program
    {
        static HttpClient client = new HttpClient();
        static string url ,cookie, website;
        static int hits;
        static void Main(string[] args)
        {
            if (args.Count() != 3)
            {
                Console.WriteLine("Usage : <website> <url> <cookie> <number of calls>");
                Console.WriteLine("Example : SameCookieText.exe http://highcpu.azurewebsites.net /home/getone ASP.NET_SessionId=gnrlwha2kz5ps3rmngyze5mp 10");

                website = "http://highcpu.azurewebsites.net";
                url = "/home/getone";
                cookie = "gnrlwha2kz5ps3rmngyze5mp";
                hits = 1;
                //return;
            }
            else
            {
                url = args[0];
                cookie = args[1];
                if (!int.TryParse(args[2], out hits))
                {
                    hits = 100;
                }
            }

            List<Task> ts = new List<Task>();
            for(int i=0;i<hits;i++)
            {
                ts.Add(Task.Factory.StartNew(() => { LoadTest(i); }));
                System.Threading.Thread.Sleep(1 * 1000);
            }

            Task.WaitAll(ts.ToArray());
            Console.ReadLine();
        }


        static void LoadTest(int i)
        {
            Console.WriteLine($"Request # {i}");

            try
            {
                IEnumerable<string> newCookie = null;
                var baseAddress = new Uri(website);

                var cookieContainer = new CookieContainer();
                using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })
                using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
                {
                    cookieContainer.Add(new Cookie("ASP.NET_SessionId", cookie, "/", "localhost"));
                    var res = client.GetAsync(url);
                    res.Wait();
                    var response = res.Result;
                    if (response.Headers.TryGetValues("Set-Cookie", out newCookie))
                    {
                        Console.WriteLine("**************NEW COOKIE FOUND*******************");
                        foreach (var s in newCookie)
                            Console.WriteLine($"{s}");
                    }

                    if (response.IsSuccessStatusCode)
                    {
                        var r = response.Content.ReadAsStringAsync();
                        Console.WriteLine($"Response # {i} : {r.Result}");
                    }
                    else
                    {
                        Console.WriteLine($"Response # {i} : {response.ReasonPhrase}");

                    }
                    //var t = client.GetStringAsync(url);
                    //t.Wait();
                    //Console.WriteLine($"Response # {i} : {t.Result}");
                }

                
                
                using (var handler = new HttpClientHandler { UseCookies = true })
                using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
                {
                    var message = new HttpRequestMessage(HttpMethod.Get, url);
                    message.Headers.Add("Cookie", cookie);
                    var t = client.SendAsync(message);
                    t.Wait();
                    var res = t.Result;
                    if (res.Headers.TryGetValues("Set-Cookie", out newCookie))
                    {
                        Console.WriteLine("**************NEW COOKIE FOUND*******************");
                        foreach(var s in newCookie)
                        Console.WriteLine($"{s}");
                    }
                    if (res.IsSuccessStatusCode)
                    {
                        var r = res.Content.ReadAsStringAsync();
                        Console.WriteLine($"Response # {i} : {r.Result}");
                    }
                    else
                    {
                        Console.WriteLine($"Response # {i} : {res.ReasonPhrase}");
                        
                    }
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine($"Exception # {i} : {ex.ToString()}");
            }
            
            Console.WriteLine($"Request # {i}....Done");
        }
    }
}
