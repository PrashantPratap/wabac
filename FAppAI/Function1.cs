//using System.Linq;
//using System.Net;
//using System.Net.Http;
//using System.Threading.Tasks;
//using Microsoft.Azure.WebJobs;
//using Microsoft.Azure.WebJobs.Extensions.Http;
//using Microsoft.Azure.WebJobs.Host;
//using Microsoft.Extensions.Logging;

//namespace FAppAI
//{

//    //https://docs.microsoft.com/en-us/azure/azure-functions/functions-monitoring#custom-telemetry-in-c-functions

//    public static class Function1
//    {
//        [FunctionName("Function1")]
//        public static async Task<HttpResponseMessage> Run([HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)]HttpRequestMessage req,
//            ExecutionContext context,
//            ILogger logger
//            //TraceWriter log
//            )
//        {

//            logger.LogInformation("");
//            //log.Info("C# HTTP trigger function processed a request.");

            

//            // parse query parameter
//            string name = req.GetQueryNameValuePairs()
//                .FirstOrDefault(q => string.Compare(q.Key, "name", true) == 0)
//                .Value;

//            if (name == null)
//            {
//                // Get request body
//                dynamic data = await req.Content.ReadAsAsync<object>();
//                name = data?.name;
//            }

//            return name == null
//                ? req.CreateResponse(HttpStatusCode.BadRequest, "Please pass a name on the query string or in the request body")
//                : req.CreateResponse(HttpStatusCode.OK, "Hello " + name);
//        }

//        static void TestAI()
//        {
//            Microsoft.ApplicationInsights.TelemetryClient telemetryClient = new Microsoft.ApplicationInsights.TelemetryClient();
//            telemetryClient.InstrumentationKey = System.Environment.GetEnvironmentVariable(" APPINSIGHTS_INSTRUMENTATIONKEY");

//            Microsoft.ApplicationInsights.DataContracts.TraceTelemetry traceTelemetry = new Microsoft.ApplicationInsights.DataContracts.TraceTelemetry();
//            traceTelemetry.Message = "from TestAI";
//            traceTelemetry.Context.Operation.Id = "";

//            telemetryClient.TrackTrace(traceTelemetry);
//        }
//    }
//}


using System;
using System.Net;
using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.DataContracts;
using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.Azure.WebJobs;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using System.Linq;

namespace functionapp0915
{
public static class HttpTrigger2
{
    private static string key = TelemetryConfiguration.Active.InstrumentationKey = 
        System.Environment.GetEnvironmentVariable(
            "APPINSIGHTS_INSTRUMENTATIONKEY", EnvironmentVariableTarget.Process);

    private static string hostname = 
        System.Environment.GetEnvironmentVariable(
            "APPSETTING_WEBSITE_SLOT_NAME", EnvironmentVariableTarget.Process);

    private static TelemetryClient telemetryClient = 
        new TelemetryClient() { InstrumentationKey = key };

    [FunctionName("HttpTrigger2")]
    public static async Task<HttpResponseMessage> Run(
        [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)]
        HttpRequestMessage req, ExecutionContext context, ILogger log)
    {

        var msg = new TraceTelemetry("msg from custom code");
        msg.Context.Operation.Id = context.InvocationId.ToString();
        telemetryClient.TrackTrace(msg);

            var msgHostName = new TraceTelemetry($"msgHostName {hostname}");
            msgHostName.Context.Operation.Id = context.InvocationId.ToString();
            telemetryClient.TrackTrace(msgHostName);

            return req.CreateResponse(HttpStatusCode.OK, "Hello " + hostname);
            


            log.LogInformation("C# HTTP trigger function processed a request.");
            DateTime start = DateTime.UtcNow;

            // parse query parameter
            string name = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "name", true) == 0)
                .Value;

            // Get request body
            dynamic data = await req.Content.ReadAsAsync<object>();

            // Set name to query string or body data
            name = name ?? data?.name;


           
            

            //// Track an Event
            //var evt = new EventTelemetry("Function called");
            //UpdateTelemetryContext(evt.Context, context, name);
            //telemetryClient.TrackEvent(evt);

            //// Track a Metric
            //var metric = new MetricTelemetry("Test Metric", DateTime.Now.Millisecond);
            //UpdateTelemetryContext(metric.Context, context, name);
            //telemetryClient.TrackMetric(metric);

            //// Track a Dependency
            //var dependency = new DependencyTelemetry
            //    {
            //        Name = "GET api/planets/1/",
            //        Target = "swapi.co",
            //        Data = "https://swapi.co/api/planets/1/",
            //        Timestamp = start,
            //        Duration = DateTime.UtcNow - start,
            //        Success = true
            //    };
            //UpdateTelemetryContext(dependency.Context, context, name);
            //telemetryClient.TrackDependency(dependency);

            return name == null
                ? req.CreateResponse(HttpStatusCode.BadRequest, 
                    "Please pass a name on the query string or in the request body")
                : req.CreateResponse(HttpStatusCode.OK, "Hello " + name);
        }

        // This correllates all telemetry with the current Function invocation
        private static void UpdateTelemetryContext(TelemetryContext context, ExecutionContext functionContext, string userName)
        {
            context.Operation.Id = functionContext.InvocationId.ToString();
            context.Operation.ParentId = functionContext.InvocationId.ToString();
            context.Operation.Name = functionContext.FunctionName;
            context.User.Id = userName;
        }
    }    
} 
 
