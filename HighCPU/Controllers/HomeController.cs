﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace HighCPU.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Ex()
        {
            throw new Exception("my exception");


            return View();
        }
    
        public ActionResult Index()
        {
            var client = new WebClient();
            string html = client.DownloadString("http://www.bing.com/ppratap");
            

            return View();
        }

        List<byte[]> wastedMemory;
        public ActionResult About()
        {
            ViewBag.Message = "High Memory page.";

            wastedMemory = new List<byte[]>();

            int i = 200000;
            while (i-- > 0)
            {
                byte[] buffer = new byte[4096]; // Allocate 4kb
                wastedMemory.Add(buffer);
            }


            return View();
        }

        public ActionResult ReleaseMemory()
        {
            ViewBag.Message = "Release Memory page.";
            wastedMemory.Clear();
            wastedMemory = null;
            return View();

        }

        bool bDone = false;

        public void CPUKill(object cpuUsage)
        {
            Parallel.For(0, 1, new Action<int>((int i) =>
            {
                Stopwatch watch = new Stopwatch();
                watch.Start();
                while (!bDone)
                {
                    if (watch.ElapsedMilliseconds > (int)cpuUsage)
                    {
                        Thread.Sleep(100 - (int)cpuUsage);
                        watch.Reset();
                        watch.Start();
                    }
                }
            }));

        }



        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            bDone = false;

            int cpuUsage = 50;
            int time = 30000;
            List<Thread> threads = new List<Thread>();
            for (int i = 0; i < Environment.ProcessorCount; i++)
            {
                Thread t = new Thread(new ParameterizedThreadStart(CPUKill));
                t.Start(cpuUsage);
                threads.Add(t);
            }
            Thread.Sleep(time);
            foreach (var t in threads)
            {
                t.Abort();
            }
            bDone = true;

            return View();
        }

        public void CPUKill2(object cpuUsage)
        {
            Parallel.For(0, 1, new Action<int>((int i) =>
            {
                client.TrackTrace($"contact2 : CPU : start {System.Threading.Thread.CurrentThread.ManagedThreadId}");

                Stopwatch watch = new Stopwatch();
                watch.Start();
                while (!bDone)
                {
                    if (watch.ElapsedMilliseconds > (int)cpuUsage)
                    {
                        Thread.Sleep(100 - (int)cpuUsage);
                        watch.Reset();
                        watch.Start();
                    }
                }

                client.TrackTrace($"contact2 : CPU : end {System.Threading.Thread.CurrentThread.ManagedThreadId}");

                client.Flush();
            }));

        }

        static Microsoft.ApplicationInsights.TelemetryClient client = new Microsoft.ApplicationInsights.TelemetryClient();


        public ActionResult Contact2()
        {
            client.TrackTrace($"contact2 : start {System.Threading.Thread.CurrentThread.ManagedThreadId}");

            ViewBag.Message = "Your contact page.";

            bDone = false;

            int cpuUsage = 50;
            int time = 30000;
            List<Thread> threads = new List<Thread>();
            for (int i = 0; i < Environment.ProcessorCount; i++)
            {
                Thread t = new Thread(new ParameterizedThreadStart(CPUKill));
                t.Start(cpuUsage);
                threads.Add(t);
            }
            Thread.Sleep(time);
            foreach (var t in threads)
            {
                t.Abort();
            }
            bDone = true;

            client.TrackTrace($"contact2 : end {System.Threading.Thread.CurrentThread.ManagedThreadId}");

            return View();
        }

    }
}