﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Kendo.Mvc.UI;
using TelerikAspNetCoreApp1.Models;
using Kendo.Mvc.Extensions;
using Microsoft.ApplicationInsights;

namespace TelerikAspNetCoreApp1.Controllers
{
    public class GridController : Controller
    {
        public ActionResult Orders_Read([DataSourceRequest]DataSourceRequest request)
        {
            char a = 'A';
            int ia = (int)a;

            Random r = new Random();
            var result = Enumerable.Range(0, 50).Select(i => new OrderViewModel
            {
                OrderID = i,
                Freight = r.Next(500, 1000),
                OrderDate = new DateTime(2016, 9, 15).AddDays(r.Next(1, 50)),
                ShipName = new string((char)(r.Next(0, 20) + ia), 300), // "ShipName " + i,
                ShipCity = $"ShipCity-{r.Next(0, 200)}",
                OrderStatus = r.Next(0, 20) % 3 ==0 ? "DONE" : "Waiting"
            });


            TelemetryClient _telemetryClient = new TelemetryClient();
            _telemetryClient.InstrumentationKey = "e8426432-f9c4-451f-a391-17e049855319";
            _telemetryClient.TrackException(new Exception("hello"));


            var dsResult = result.ToDataSourceResult(request);
            return Json(dsResult);

            
        }
    }
}
