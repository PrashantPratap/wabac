﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TelerikAspNetCoreApp1.Models;

namespace TelerikAspNetCoreApp1.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            ViewData["ClassificationData"] = "{\"Classifications\":[{\"TopLevel\":\"CSS\",\"SubLevel\":[{\"Name\":\"Misroute\"},{\"Name\":\"Waiting on collaboration with other teams\"},{\"Name\":\"FDR Missed (or close to 24hours) before taking ownership\"},{\"Name\":\"Engineer busy with other cases\"},{\"Name\":\"Engineer missed to mark the case as SD-PC\"},{\"Name\":\"Waiting for response from consult request (Ex: TAs, DLs)\"},{\"Name\":\"Tooling issues (MSSolve, Skype)\"},{\"Name\":\"Case opened outside business hours\"}]},{\"TopLevel\":\"Customer\",\"SubLevel\":[{\"Name\":\"Waiting for customer(for data)\"},{\"Name\":\"Waiting for customer(for better problem description)\"},{\"Name\":\"Waiting on customer for follow-up\"}]},{\"TopLevel\":\"PG\",\"SubLevel\":[{\"Name\":\"Microsoft Product Issue\"},{\"Name\":\"Missing Documentation\"},{\"Name\":\"Not enough logs\"},{\"Name\":\"Need Tools to collect data and analyze\"},{\"Name\":\"Pending CRI / Operation\"}]}]}";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
        public IActionResult FilterName()
        {
            return View(new Models.FilterViewModel());
        }

        public IActionResult QueueName()
        {
            return View(new Models.FilterViewModel());
        }

        static WizardViewModel _wvm = new WizardViewModel();
        public IActionResult Wizard()
        {
            return View(_wvm);
        }
        [HttpPost]
        public IActionResult Wizard(WizardViewModel vm, IFormCollection fc)
        {
            return RedirectToAction(nameof(Index));
        }
        public IActionResult PageOne()
        {
            return View(_wvm);
        }
        public IActionResult PageTwo()
        {
            return View(_wvm);
        }

        public IActionResult PageThree()
        {
            return View(_wvm);
        }

        public IActionResult TimePicker()
        {
            TimePickerModel timePickerModel = new TimePickerModel();
            timePickerModel.OnlyTime = DateTime.UtcNow;
            return View(timePickerModel);
        }
        [HttpPost]
        public IActionResult TimePicker(TimePickerModel vm, IFormCollection fc)
        {
            return View(vm);
        }

        private List<OmegaModel> GetModel()
        {
            List<OmegaModel> m = new List<OmegaModel>()
            {
                new OmegaModel()
                {
                     Name = "TestOne", Label = "Test One", ControlType= enumControlType.Text, Value="one"
                },
                new OmegaModel()
                {
                     Name = "TestTwo", Label = "Test Two", ControlType= enumControlType.Text, Value="two"
                },
                new OmegaModel()
                {
                     Name = "TestThree", Label = "Test Three", ControlType= enumControlType.Area, Value="three"
                },
                new OmegaModel()
                {
                     Name = "TestFour", Label = "Test Four", ControlType= enumControlType.Area, Value="four"
                },
                new OmegaModel()
                {
                     Name = "TestFive", Label = "Test Five", ControlType= enumControlType.Boolean, Value=true.ToString().ToLower()
                }
            };
            return m;
        }

        public IActionResult TextOne()
        {
            return View(GetModel());
        }

        [HttpPost]
        public IActionResult TextOne(IFormCollection fc)
        {
            var m = GetModel();

            foreach(var ctrl in m)
            {
                ctrl.Value = fc[ctrl.Name];
            }

            return View(m);
        }

        public IActionResult TextTwo()
        {
            var m = GetModel();
            ViewData["TWO"] = OmegaModel.Render(m, "/Home/TextTwo");
            return View();
        }
    }
}
