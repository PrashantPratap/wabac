﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TelerikAspNetCoreApp1.Models
{
    public class FilterViewModel
    {
        [Display(Name = "Filter ID")]
        public int FilterID { get; set; }

        [Required]
        [Display(Name = "Filter Name", Prompt = "cases to review")]
        public string FilterName { get; set; }

        [Display(Name = "Limit number of cases to")]
        [Range(10, 1000,
      ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int LimitNumberOfCases { get; set; }
        public int FilterType { get; set; } // 0:default ; 1:case numbers search filter ; 2:email alerts

        [Display(Name = "Filter Owner")]
        public string FilterOwner { get; set; }

        //        public string ReviewType { get; set; } //Delivery,SPM
        public string ReviewMetric { get; set; } //FDR,FWR

        [Display(Name = "Queue Name", Prompt = "POD Azure PaaS Developer ; AppServices%")]
        //[RegularExpression(@"^[a-zA-Z]", ErrorMessage =  @"please remove % from queue name")]
        public string QueueName { get; set; }

        [Display(Name = "Support Topic", Prompt = "Routing Azure Virtual Machine V3 ; Virtual%")]
        public string SupportTopics { get; set; }
        [Display(Name = "Routing Product", Prompt = "Azure Storage ; SQL%")]
        public string RoutingProduct { get; set; }
        [Display(Name = "Show only these cases (all other filter options will be ignored)")]
        public string SearchCaseNumbers { get; set; }
    }
}
