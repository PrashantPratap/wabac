﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TelerikAspNetCoreApp1.Models
{
    public class WizardViewModel
    {
        public string Page1TextBox { get; set; }
        public bool Page2CheckBox { get; set; }
        public string Page3TextBox { get; set; }
    }
}
