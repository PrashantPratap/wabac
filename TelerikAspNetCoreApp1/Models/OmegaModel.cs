﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelerikAspNetCoreApp1.Models
{
    public enum enumControlType
    {
        Text,
        Area,
        Boolean,
        DropDownSingleSelection,
        DropDownMultipleSelection,
        CascadeTopLevel,
        CascadeSubLevel,
        CascadeURL
    }
    public class OmegaModel
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string Label { get; set; }
        public enumControlType ControlType { get; set; }
        public string DataOptions { get; set; }

        public static string Render(List<OmegaModel> ctrls, string postURL)
        {
            var jsSB = new StringBuilder();
            jsSB.Append("<script> function OmegaSubmit() { var reviewData = {");

            var sb = new StringBuilder();

            foreach (var ctrl in ctrls)
            {
                sb.Append("<div class='form-group'><label class='control-label col-sm-4'>");

                switch (ctrl.ControlType)
                {
                    case enumControlType.Text:
                        {
                            sb.Append($"{ctrl.Label}</label> <div class='col-sm-8 col-md-6'><input class='k-textbox' id='{ctrl.Name}' name='{ctrl.Name}' value='{ctrl.Value}' type='text' />");
                            jsSB.Append($"{ctrl.Name}:$('#{ctrl.Name}').val(),");
                            break;
                        }
                    case enumControlType.Area:
                        {
                            sb.Append($"{ctrl.Label}</label> <div class='col-sm-8 col-md-6'> <textarea class='k-textbox'  id='{ctrl.Name}' name='{ctrl.Name}' rows='4' cols='500' />{ctrl.Value}</textarea>");
                            jsSB.Append($"{ctrl.Name}:$('#{ctrl.Name}').val(),");
                            break;
                        }
                    case enumControlType.Boolean:
                        {
                            sb.Append($"{ctrl.Label}</label> <div class='col-sm-8 col-md-6'> <input id='{ctrl.Name}' name='{ctrl.Name}' type='checkbox' value='{ctrl.Value}' />");
                            jsSB.Append($"{ctrl.Name}:$('#{ctrl.Name}').val(),");
                            break;
                        }
                }
                sb.Append("</div></div>");
            }

            jsSB.Remove(jsSB.Length - 1, 1); // remove the last ","
            jsSB.Append("};"); // closing review data class
            jsSB.Append("alert(JSON.stringify($('#TestOne').val()));"); // test code
            jsSB.Append("alert(JSON.stringify(reviewData));"); // test code

            jsSB.Append($"$.post('{postURL}', {{ CaseNumber: SRNumber, FilterID: FilterID , ReviewerRole: ReviewerRole, ReviewMetric: ReviewMetric, ReviewData: reviewData }}, function(data) {{console.log(JSON.stringify(data));var grid = $(\"#grid\").data(\"kendoGrid\");grid.dataSource.read();}}, \"json\");");
            jsSB.Append("} </script>"); // closing function 
            
            return sb.ToString() + jsSB.ToString();
        }
    }
    
    [HtmlTargetElement("omega")]
    public class OmegaTagHelper : TagHelper
    {
        [HtmlAttributeName("name")]
        public string Name { get; set; }
        [HtmlAttributeName("value")]
        public string Value { get; set; }
     
        [HtmlAttributeName("label")]
        public string Label { get; set; }

        [HtmlAttributeName("controltype")]
        public enumControlType ControlType { get; set; }

        [HtmlAttributeName("dataoptions")]
        public string DataOptions { get; set; }

        public override void Process(
            TagHelperContext context,
            TagHelperOutput output)
        {
            var sb = new StringBuilder();

            sb.Append("<div class='form-group'><label class='control-label col-sm-4'>");

            switch(this.ControlType)
            {
                case enumControlType.Text:
                    {
                        sb.Append($"{this.Label}</label> <div class='col-sm-8 col-md-6'><input class='k-textbox' id='{this.Name}' name='{this.Name}' value='{this.Value}' type='text' />");
                        break;
                    }
                case enumControlType.Area:
                    {
                        sb.Append($"{this.Label}</label> <div class='col-sm-8 col-md-6'> <textarea class='k-textbox'  id='{this.Name}' name='{this.Name}' rows='4' cols='500' />{this.Value}</textarea>");
                        break;
                    }
                case enumControlType.Boolean:
                    {
                        sb.Append($"{this.Label}</label> <div class='col-sm-8 col-md-6'> <input id='{this.Name}' name='{this.Name}' type='checkbox' value='{this.Value}' />");
                        break;
                    }
            }
            sb.Append("</div></div>");

            //output.PreContent.SetHtmlContent(sb.ToString());
            output.PostContent.SetHtmlContent(sb.ToString());
        }
    }
}


/*
https://tahirnaushad.com/2017/08/27/asp-net-core-2-0-mvc-custom-tag-helpers/
https://demos.telerik.com/kendo-ui/switch/index

 <div class="demo-section">
        <div class="settings-head"></div>
        <ul>
            <li>Notifications <input type="checkbox" id="notifications-switch" aria-label="Notifications Switch" checked="checked" /></li>
            <li>Send notifications <input id="mail-switch" aria-label="Mail Switch" /></li>
            <li>Always visible <input id="visible-switch" aria-label="Visible Switch" /></li>
            <li>Display real name <input id="name-switch" aria-label="Name Switch" /></li>
        </ul>
    </div>

    <style>
        .demo-section ul {
            margin: 0;
            padding: 0;
        }

            .demo-section ul li {
                margin: 0;
                padding: 10px 10px 10px 20px;
                min-height: 28px;
                line-height: 28px;
                vertical-align: middle;
                border-top: 1px solid rgba(128,128,128,.5);
            }

        .demo-section {
            min-width: 220px;
            margin-top: 50px;
            padding: 0;
        }

            .demo-section ul li .k-switch {
                float: right;
            }

        .settings-head {
            height: 66px;
            background: url('../content/web/switch/settings-title.png') no-repeat 20px 50% #2db245;
        }
    </style>
 
 
 */
