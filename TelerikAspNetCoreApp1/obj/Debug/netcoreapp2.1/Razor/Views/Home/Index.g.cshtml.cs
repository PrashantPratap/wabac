#pragma checksum "D:\Projects\Wabac\TelerikAspNetCoreApp1\Views\Home\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "cb99caa45f4f53eb42cde8a98e3d02d6d57ac0d4"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Home/Index.cshtml", typeof(AspNetCore.Views_Home_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\Projects\Wabac\TelerikAspNetCoreApp1\Views\_ViewImports.cshtml"
using TelerikAspNetCoreApp1;

#line default
#line hidden
#line 2 "D:\Projects\Wabac\TelerikAspNetCoreApp1\Views\_ViewImports.cshtml"
using Kendo.Mvc.UI;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"cb99caa45f4f53eb42cde8a98e3d02d6d57ac0d4", @"/Views/Home/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"4efe3be673baec3f9e7a237ce42ca53f9ff9b64a", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("img-responsive"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("alt", new global::Microsoft.AspNetCore.Html.HtmlString("200x200"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Images/200.png"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("name", "PrimaryButton", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("tag", "span", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("textButton k-primary"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_6 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("name", "TextButton", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_7 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("textButton"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_8 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("name", "Button", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        private global::Kendo.Mvc.TagHelpers.ButtonTagHelper __Kendo_Mvc_TagHelpers_ButtonTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 1 "D:\Projects\Wabac\TelerikAspNetCoreApp1\Views\Home\Index.cshtml"
  
    ViewData["Title"] = "Home Page";

#line default
#line hidden
            BeginContext(45, 78, true);
            WriteLiteral("\r\n<div class=\"row\">\r\n    <div class=\"col-xs-4 col-md-3 placeholder\">\r\n        ");
            EndContext();
            BeginContext(123, 65, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "7488b518824b4bbf9d0a3efbf4ee46a5", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(188, 671, true);
            WriteLiteral(@"
    </div>
    <div class=""col-xs-12 col-md-9"">
        <div class=""row"">
            <div class=""col-xs-12 col-md-12"">
                <h2>Lorem ipsum...</h2>
            </div>
        </div>
        <div class=""row"">
            <div class=""col-xs-12 col-md-4"">
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                    galley of type and scrambled it to make a type specimen book.
                </p>

                <p>
                    ");
            EndContext();
            BeginContext(859, 104, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("kendo-button", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "ff9d329c69fe4ec9a28445367c835186", async() => {
                BeginContext(934, 14, true);
                WriteLiteral("Primary Button");
                EndContext();
            }
            );
            __Kendo_Mvc_TagHelpers_ButtonTagHelper = CreateTagHelper<global::Kendo.Mvc.TagHelpers.ButtonTagHelper>();
            __tagHelperExecutionContext.Add(__Kendo_Mvc_TagHelpers_ButtonTagHelper);
            __Kendo_Mvc_TagHelpers_ButtonTagHelper.Name = (string)__tagHelperAttribute_3.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_3);
            __Kendo_Mvc_TagHelpers_ButtonTagHelper.Tag = (string)__tagHelperAttribute_4.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_4);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(963, 485, true);
            WriteLiteral(@"
                </p>
            </div>

            <div class=""col-xs-12 col-md-4"">
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                    galley of type and scrambled it to make a type specimen book.
                </p>
                <p>
                    ");
            EndContext();
            BeginContext(1448, 83, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("kendo-button", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "99c1d7d70838462990c983b3c20d1061", async() => {
                BeginContext(1510, 6, true);
                WriteLiteral("Button");
                EndContext();
            }
            );
            __Kendo_Mvc_TagHelpers_ButtonTagHelper = CreateTagHelper<global::Kendo.Mvc.TagHelpers.ButtonTagHelper>();
            __tagHelperExecutionContext.Add(__Kendo_Mvc_TagHelpers_ButtonTagHelper);
            __Kendo_Mvc_TagHelpers_ButtonTagHelper.Name = (string)__tagHelperAttribute_6.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_6);
            __Kendo_Mvc_TagHelpers_ButtonTagHelper.Tag = (string)__tagHelperAttribute_4.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_4);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_7);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1531, 487, true);
            WriteLiteral(@"
                </p>

            </div>
            <div class=""col-xs-12 col-md-4"">
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                    galley of type and scrambled it to make a type specimen book.
                </p>

                <p>
                    ");
            EndContext();
            BeginContext(2018, 79, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("kendo-button", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "eb18083da6f14bcbaa517c3a7d52bf68", async() => {
                BeginContext(2076, 6, true);
                WriteLiteral("Button");
                EndContext();
            }
            );
            __Kendo_Mvc_TagHelpers_ButtonTagHelper = CreateTagHelper<global::Kendo.Mvc.TagHelpers.ButtonTagHelper>();
            __tagHelperExecutionContext.Add(__Kendo_Mvc_TagHelpers_ButtonTagHelper);
            __Kendo_Mvc_TagHelpers_ButtonTagHelper.Name = (string)__tagHelperAttribute_8.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_8);
            __Kendo_Mvc_TagHelpers_ButtonTagHelper.Tag = (string)__tagHelperAttribute_4.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_4);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_7);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2097, 301, true);
            WriteLiteral(@"
                </p>

            </div>
        </div>
    </div>

</div>

<div class=""row"">
    <div>
        <a href=""#"" class=""k-button"" id=""save"">Save State</a>
        <a href=""#"" class=""k-button"" id=""load"">Load State</a>
    </div>
    <div class=""col-xs-18 col-md-12"">
        ");
            EndContext();
            BeginContext(2400, 1426, false);
#line 62 "D:\Projects\Wabac\TelerikAspNetCoreApp1\Views\Home\Index.cshtml"
    Write(Html.Kendo().Grid<TelerikAspNetCoreApp1.Models.OrderViewModel>
            ()
            .Name("grid")
            .Columns(columns =>
            {
                columns.Bound(p => p.OrderID).Filterable(false);
                columns.Bound(p => p.Freight);
                columns.Bound(p => p.OrderDate).Format("{0:MM/dd/yyyy}");
                columns.Bound(p => p.ShipName);
                columns.Bound(p => p.ShipCity);
                columns.Bound(p => p.OrderStatus)
                .ClientGroupHeaderTemplate("#= value # (Order Count: #= count#)")
                .ClientFooterTemplate("<span id='FooterCompletedCount'></span>")
                .ClientHeaderTemplate("<span id='HeaderCompletedCount'></span>");
            })
            .ToolBar(tools => tools.Excel())
            .Pageable()
            .Sortable()
            .Scrollable()
            .Filterable()
            .Groupable()
            .ColumnMenu()
            .Reorderable(r => r.Columns(true))
            .Resizable(r => r.Columns(true))
            .HtmlAttributes(new { style = "height:550px;" })
            .Events(e => e.DataBound("onDataBound"))
            .DataSource(dataSource => dataSource
            .Ajax()
            .PageSize(20)
            .Read(read => read.Action("Orders_Read", "Grid"))
            .Aggregates(ag => { ag.Add(o => o.OrderStatus).Count(); })
            )
        );

#line default
#line hidden
            EndContext();
            BeginContext(3827, 12, true);
            WriteLiteral("\r\n\r\n        ");
            EndContext();
            BeginContext(3841, 587, false);
#line 96 "D:\Projects\Wabac\TelerikAspNetCoreApp1\Views\Home\Index.cshtml"
    Write(Html.Kendo().ContextMenu()
                                        .Name("contextmenu")
                                        .Target("#grid")
                                        .Orientation(ContextMenuOrientation.Vertical)
                                        .Items(items =>
                                        {
                                            items.Add()
                                                .Text("Get Row");
                                        })
                                        .Events(e => e.Select("onSelect"))
        );

#line default
#line hidden
            EndContext();
            BeginContext(4429, 1658, true);
            WriteLiteral(@"
    </div>
</div>
<script>
    function onSelect(e) {
        var item = e.item.innerHTML;
        var grid = $(""#grid"").data(""kendoGrid"");
        var model = grid.dataItem(e.target);

        console.log(""Context menu selected - "" + item + "" model "" + JSON.stringify(model));
        alert(model);
    }

    var x = 0;

    function onDataBound(e) {

        var grid = $(""#grid"").data(""kendoGrid"");

        var options = localStorage[""kendo-grid-options""];

        //alert(""options "" + options);
        if (x ==0 && options != null && options.length > 0) {
            alert(""loading..."")
            console.log(options);
            grid.setOptions(JSON.parse(options));
            x = 1;
        }


        var data = this.dataSource.data();
        var completed = 0;
        for (var i = 0; i < data.length; i++) {
            if (data[i].OrderStatus === ""DONE"") {
                completed++;
            }
        }

        $(""#FooterCompletedCount"").text(""Completed o");
            WriteLiteral(@"rders: "" + completed);
        $(""#HeaderCompletedCount"").text(""Completed orders: "" + completed);
    }


    $(function () {
        var grid = $(""#grid"").data(""kendoGrid"");

        $(""#save"").click(function (e) {
            e.preventDefault();
            localStorage[""kendo-grid-options""] = kendo.stringify(grid.getOptions());
        });

        $(""#load"").click(function (e) {
            e.preventDefault();
            var options = localStorage[""kendo-grid-options""];
            if (options) {
                grid.setOptions(JSON.parse(options));
            }
        });
    });
    

 </script>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
