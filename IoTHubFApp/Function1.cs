using IoTHubTrigger = Microsoft.Azure.WebJobs.EventHubTriggerAttribute;

using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Azure.EventHubs;
using System.Text;
using System.Net.Http;
using Microsoft.Extensions.Logging;

namespace IoTHubFApp
{
    public static class Function1
    {
        private static HttpClient client = new HttpClient();

        [FunctionName("Function1")]
        public static void Run([IoTHubTrigger("deviceid", Connection = "deviceid_events_IOTHUB")]EventData message, ILogger log)
        {
            log.LogInformation($"C# IoT Hub trigger function processed a message: {Encoding.UTF8.GetString(message.Body.Array)}");

            log.LogInformation($"EvnetData: {Newtonsoft.Json.JsonConvert.SerializeObject(message)}");
        }
    }
}

/*
 module.exports = function (context, IoTHubMessages) {
    context.log(`JavaScript eventhub trigger function called for message array: ${JSON.stringify(IoTHubMessages)}`);

    context.log(`context: ${JSON.stringify(context)}`);
    
   // IoTHubMessages.forEach(message => {
   //     context.log(`Processed message: ${message}`);
   // });

    context.done();
};
 */
