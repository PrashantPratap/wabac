﻿//using System;
//using System.Threading.Tasks;
//using StackExchange.Redis;
//using Newtonsoft.Json;

//class one
//{
//    public static void Run(string myQueueItem, TraceWriter log)
//    {
//        log.Info($"C# ServiceBus queue trigger function processed message: {myQueueItem}");

//        var msg = JsonConvert.DeserializeObject<MyOutput>(myQueueItem);

//        if (msg == null)
//        {
//            log.Info("failed to convert msg to MyOutput");
//            return;
//        }

//        IDatabase cache = Connection.GetDatabase();
//        cache.StringSet(msg.devicename, myQueueItem);
//    }

//    private static Lazy<ConnectionMultiplexer> lazyConnection = new Lazy<ConnectionMultiplexer>(() =>
//    {
//        return ConnectionMultiplexer.Connect("WabacRedis.redis.cache.windows.net:6380,password=bs9pnbixPXMHJls2RZNlPbwmWukqB+pMFd1UAh89Jcs=,ssl=True,abortConnect=False");
//    });

//    public static ConnectionMultiplexer Connection
//    {
//        get
//        {
//            return lazyConnection.Value;
//        }
//    }

//    class MyOutput
//    {
//        //{"devicename":"Console App - 8","avgtemp":62.0,"avgspeed":207.0}

//        public string devicename { get; set; }
//        public double avgtemp { get; set; }
//        public double avgspeed { get; set; }
//    }
//}