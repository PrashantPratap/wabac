﻿
/*

    using NLog;
using NLog.Common;
using NLog.Config;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nLog
{
    class Program
    {
        static void Main(string[] args)
        {
            LogManager.ThrowExceptions = true;
            LogManager.ThrowConfigExceptions = true;
            //InternalLogger.LogToConsole = true;
            InternalLogger.LogFile = "log.txt";
            InternalLogger.LogLevel = LogLevel.Trace;

            // config
            //LogManager.Configuration = new XmlLoggingConfiguration("D:\\home\\site\\wwwroot\\TimerTriggerCSharp3\\NLog.config");
            //logger = LogManager.GetCurrentClassLogger();


#if DEBUG1 //FILE_TARGET
            NLog.Targets.FileTarget target = new NLog.Targets.FileTarget("file_target");
            target.FileName = "logfile.txt";
            NLog.Config.SimpleConfigurator.ConfigureForTargetLogging(target, LogLevel.Trace);

            Logger logger = LogManager.GetLogger("file_target");
#elif DEBUG2
            NLog.Targets.DatabaseTarget target = new NLog.Targets.DatabaseTarget("db_target");
            NLog.Targets.DatabaseParameterInfo param;

            //target.DBProvider = "System.Data.SqlClient";
            target.DBHost = "tacasessrv.database.windows.net";
            target.DBUserName = "dbuser";
            target.DBPassword = "Micr0s0ft";
            target.DBDatabase = "wabacdb";
            target.CommandText = "insert into LogTable(time_stamp,level,logger,message) values(@time_stamp, @level, @logger, @message);";

            param = new NLog.Targets.DatabaseParameterInfo();
            param.Name = "@time_stamp";
            param.Layout = "${date}";
            target.Parameters.Add(param);

            param = new NLog.Targets.DatabaseParameterInfo();
            param.Name = "@level";
            param.Layout = "${level}";
            target.Parameters.Add(param);

            param = new NLog.Targets.DatabaseParameterInfo();
            param.Name = "@logger";
            param.Layout = "${logger}";
            target.Parameters.Add(param);

            param = new NLog.Targets.DatabaseParameterInfo();
            param.Name = "@message";
            param.Layout = "${message}";
            target.Parameters.Add(param);


            NLog.Config.SimpleConfigurator.ConfigureForTargetLogging(target, LogLevel.Trace);
            Logger logger = LogManager.GetLogger("db_target");

#else
            Logger logger = LogManager.GetCurrentClassLogger();
#endif

            logger.Trace("Sample trace message");
            logger.Debug("Sample debug message");
            logger.Info("Sample informational message");
            logger.Warn("Sample warning message");
            logger.Error("Sample error message");
            logger.Fatal("Sample fatal error message");

            LogManager.Flush();
        }
    }





}


*/