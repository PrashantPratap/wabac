
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using System;
using Microsoft.WindowsAzure.Storage.Table;
using Microsoft.WindowsAzure.Storage;
using System.Collections.Generic;

namespace FAppStorage
{
    public static class Function1
    {
        [FunctionName("Function1")]
        public static IActionResult Run([HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)]HttpRequest req,
            [Table("FunctionOutput", "appstorageawahijoxtz73o_STORAGE")] IAsyncCollector<MyTableItem> outputTable,
            TraceWriter log)

        //public static IActionResult Run([HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)]HttpRequest req,
        //public static IActionResult Run(HttpRequest req,
        //    IAsyncCollector<MyTableItem> outputTable,
        //    TraceWriter log)
        //            Error indexing method 'Functions.Function1'. Microsoft.Azure.WebJobs.Host: Can't bind Table to type 'Microsoft.Azure.WebJobs.IAsyncCollector`1[FAppStorage.MyTableItem]

        //public static IActionResult Run(HttpRequest req,
        //ICollector<MyTableItem> outputTable,
        //TraceWriter log)
        ////            Error indexing method 'Functions.Function1'. Microsoft.Azure.WebJobs.Host: Can't bind Table to type 'Microsoft.Azure.WebJobs.ICollector`1[FAppStorage.MyTableItem]'


        //public static IActionResult Run(HttpRequest req,
        //out MyTableItem outputTable,
        //TraceWriter log)
        {
            log.Info("C# HTTP trigger function processed a request.");

            string name = req.Query["name"];

            string requestBody = new StreamReader(req.Body).ReadToEnd();
            dynamic data = JsonConvert.DeserializeObject(requestBody);
            name = name ?? data?.name;

            outputTable.AddAsync(new MyTableItem() { Name = $"Hello {name}", RowKey = Guid.NewGuid().ToString(), PartitionKey = "one" });
            // outputTable.Add(new MyTableItem() { Name = $"Hello {name}", RowKey = Guid.NewGuid().ToString(), PartitionKey = "one" });
            //outputTable = new MyTableItem() { Name = $"Hello {name}", RowKey = Guid.NewGuid().ToString(), PartitionKey = "one" };

            return name != null
                ? (ActionResult)new OkObjectResult($"Hello, {name}")
                : new BadRequestObjectResult("Please pass a name on the query string or in the request body");
        }
    }

    public class MyTableItem
    {
        public string PartitionKey { get; set; }
        public string RowKey { get; set; }
        public string Name { get; set; }
    }

    //public class MyTableItem : TableEntity
    //{
    //    public string Name { get; set; }

    //}
}
