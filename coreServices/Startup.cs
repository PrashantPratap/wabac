﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using coreServices.Services;
using coreServices.Model;

namespace coreServices
{
    public class Startup
    {
public Startup(IHostingEnvironment env)
{
    var builder = new ConfigurationBuilder()
        .SetBasePath(env.ContentRootPath)
        .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
        .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
        .AddEnvironmentVariables();
    Configuration = builder.Build();
}

public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddLogging();

        // Add framework services.
        services.AddMvc();

        //services.Configure<MyConfigSection>(Configuration.GetSection("MyConfig"));
        services.Configure<MyConfigSection>(Configuration);

        services.AddSingleton<IMyService>(new MyService());

        //services.AddSingleton<IMyService>(x => { return new MyService(() => { return new MySubService(); }); });

        services.AddSingleton<IMyHttpClient>(x => { return (IMyHttpClient)new MyHttpClient(); });
        services.AddSingleton<IMyCacheService>(x => { return (IMyCacheService)new MyRedisCacheService("wabac.redis.cache.windows.net:6380,password=+nnv0pEutXPeIGuClf8zWXoY7I9U1fCvVjTvjDA4wsE=,ssl=True,abortConnect=False"); });
    }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(LogLevel.Trace);
            loggerFactory.AddDebug();

            // Exception details
            app.UseDeveloperExceptionPage();
          
            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
