using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using coreServices.Services;
using Microsoft.Extensions.Logging;
using System.Net.Http;

namespace coreServices.Controllers
{
    public class MyController : Controller
    {
        private readonly IMyHttpClient httpClient;
        private readonly IMyCacheService redicCache;
        private readonly ILogger<MyController> logger;

        public MyController(ILogger<MyController> logger, IMyHttpClient httpClient, IMyCacheService redicCache)
        {
            this.logger = logger;
            this.httpClient = httpClient;
            this.redicCache = redicCache;
        }

        public async Task<IActionResult> Index()
        {
            logger.LogTrace($"from My/Index at {DateTime.UtcNow.ToString()}");
            redicCache.Put("MY+Index", DateTime.UtcNow.ToString());
            var x = await httpClient.SendAsync(new HttpRequestMessage() { RequestUri = new Uri("http://bing123.com"), Method = HttpMethod.Get    });
            return View();
        }
    }
}