﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using coreServices.Services;
using coreServices.Model;
using Microsoft.Extensions.Options;

namespace coreServices.Controllers
{
public class HomeController : Controller
{
private readonly MyConfigSection myConfig;
private readonly IMyService myService;
public HomeController(IMyService myServiceArg, IOptions<MyConfigSection> config)
{
    myConfig = config.Value;
    myService = myServiceArg;
}

public async Task<ActionResult> Index()
{
    ViewBag.One = myConfig.One;
    //myService.DoWork("from Index");
    return View();

            var types = System.Reflection.Assembly.GetEntryAssembly().GetTypes();
        }



        public IActionResult About()
        {
            myService.DoWork("from About");

            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public async Task<IActionResult> Contact()
        {
            await myService.DoWork2("hello");
            myService.DoWork("from Contact");
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Exception()
        {
            throw new Exception("hello");
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
