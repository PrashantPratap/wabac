﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace coreServices.Model
{

  //    "MyConfig": {
  //  "One": "1",
  //  "Two": "2"
  //}

    public class MyConfigSection
    {
        public string MyConfig2 { get; set; }
        public string One { get; set; }
        public string Two { get; set; }
    }
    
}
