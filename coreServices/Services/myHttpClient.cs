﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace coreServices.Services
{
    public interface IMyHttpClient
    {
        Task<HttpResponseMessage> SendAsync(HttpRequestMessage request);
    }
    public class MyHttpClient : IMyHttpClient
    {
        private readonly HttpClient client = new HttpClient();

        public async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request)
        {
            HttpResponseMessage response = null;
            response = await client.SendAsync(request);
            return response;
        }
    }
}
