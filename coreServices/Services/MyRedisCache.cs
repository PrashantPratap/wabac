﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace coreServices.Services
{
    public class MyRedisCache
    {
        public static void LogMsg(string msg)
        {
            ConnectionMultiplexer redis = ConnectionMultiplexer.Connect("wabac.redis.cache.windows.net:6380,password=+nnv0pEutXPeIGuClf8zWXoY7I9U1fCvVjTvjDA4wsE=,ssl=True,abortConnect=False");
            IDatabase db = redis.GetDatabase();

            int PID = System.Diagnostics.Process.GetCurrentProcess().Id;
            db.ListLeftPush("CORE", $"PID={PID} : Time={DateTime.UtcNow.ToString()} : {msg}");
            
        }
    }
}
