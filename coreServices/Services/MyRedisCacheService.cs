﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace coreServices.Services
{
    public interface IMyCacheService
    {
        void Put(string key, object value);
        T Get<T>(string key);
        Task<T> GetAsync<T>(string key);
    }
    public sealed class MyRedisCacheService : IMyCacheService
    {
        private readonly ILogger<MyRedisCacheService> logger;
        private Func<IDatabase> GetDatabaseFunction = () => RedisConnectionHelper.Connection.GetDatabase();

        private IDatabase Cache => GetDatabaseFunction();

        public MyRedisCacheService( string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                throw new ArgumentNullException(nameof(connectionString));

            RedisConnectionHelper.Initialize(connectionString);

            System.Threading.Thread.Sleep(60 * 1000);
            
        }
        public MyRedisCacheService(ILogger<MyRedisCacheService> logger, string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                throw new ArgumentNullException(nameof(connectionString));

            RedisConnectionHelper.Initialize(connectionString);

            this.logger = logger;
        }

        public void Put(string key, object value)
        {
            if (string.IsNullOrWhiteSpace(key))
                throw new ArgumentNullException("key");

            try
            {
               Cache.StringSet(key, JsonConvert.SerializeObject(value));
            }
            catch (RedisException redisEx)
            {
                //logger.LogCritical($"Unable to connect to Redis Cache...{redisEx.ToString()}");
            }
        }
        public T Get<T>(string key)
        {
            if (string.IsNullOrWhiteSpace(key))
                throw new ArgumentNullException("key");

            try
            {
                var value = Cache.StringGet(key);
                return value.HasValue ? JsonConvert.DeserializeObject<T>(value, new JsonSerializerSettings()
                {
                    TypeNameHandling = TypeNameHandling.Objects
                }) : default(T);
            }
            catch (RedisException redisEx)
            {
                //logger.LogCritical( $"Unable to connect to Redis Cache...{redisEx.ToString()}");
            }

            return default(T);

        }


        public async Task<T> GetAsync<T>(string key)
        {
            if (string.IsNullOrWhiteSpace(key))
                throw new ArgumentNullException("key");

            try
            {
                var value = await Cache.StringGetAsync(key);
                return value.HasValue ? JsonConvert.DeserializeObject<T>(value, new JsonSerializerSettings()
                {
                    TypeNameHandling = TypeNameHandling.Objects
                }) : default(T);
            }
            catch (RedisException redisEx)
            {
                //logger.LogCritical($"Unable to connect to Redis Cache...{redisEx.ToString()}");
            }

            return default(T);
        }
    }



    public static class RedisConnectionHelper
    {
        public static void Initialize(string connectionString)
        {
            if (lazyConnection == null)
            {
                lazyConnection = new Lazy<ConnectionMultiplexer>(() => {
                    return ConnectionMultiplexer.Connect(connectionString);
                });
            }
        }

        //Lazy = only initialized on first use
        private static Lazy<ConnectionMultiplexer> lazyConnection = null;

        public static ConnectionMultiplexer Connection => lazyConnection.Value;
    }


}
