﻿using Microsoft.ApplicationInsights;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace coreServices.Services
{
    public class Logging
    {
        public static TelemetryClient telemetry = new TelemetryClient();

        static Logging()
        {
            telemetry.InstrumentationKey = "2a952e69-fa62-4222-9c7b-4a198904d766";
            PID = System.Diagnostics.Process.GetCurrentProcess().Id;
        }

        private static int PID;
        static public void LogMsg(string msg)
        {
            Logging.telemetry.TrackTrace($"PID={PID} : Time={DateTime.UtcNow.ToString()} : {msg}");
        }
    }
    public interface IMyService
    {
        Task<bool> DoWork2(string workItem);

        bool DoWork(string workItem);
    }

    public interface IMySubService
    {
        Task<bool> DoSubWork2(string workItem);

        bool DoSubWork(string workItem);
    }
    public class MySubService : IMySubService
    {
        public bool DoSubWork(string workItem)
        {
            MyRedisCache.LogMsg($"in DoWork of MySubService {workItem}");
            return true;
        }

        public Task<bool> DoSubWork2(string workItem)
        {
            return new Task<bool>(() =>
            {
                MyRedisCache.LogMsg($"in DoWork2 of MySubService {workItem}");
                return true;
            });
        }
    }
    public class MyService : IMyService
    {
        static AutoResetEvent are = new AutoResetEvent(false);

        //Func<IMySubService> subFunction;

        //public MyService(Func<IMySubService> tempFunction)
        //{
        //    subFunction = tempFunction;
        //}
        public  MyService()
        {
            MyRedisCache.LogMsg("in ctor of MyService");
            //are.WaitOne();
            //throw new Exception("from MyService ctor : " + DateTime.UtcNow.ToString());
            //var t = DoWork3("ctor");
            //bool x = t.Result;
        }
        //bool IMyService.DoWork(string workItem)
        //{
        //    MyRedisCache.LogMsg($"in DoWork of MyService {workItem}");
        //    return true;
        //}
        //    Task<bool> IMyService.DoWork2(string workItem)
        //{
        //    return new Task<bool>(() => 
        //    {
        //        MyRedisCache.LogMsg($"in DoWork2 of MyService {workItem}");
        //        return true;
        //    });
        //}

        Task<bool> DoWork3(string workItem)
        {
            return new Task<bool>(() =>
            {
                MyRedisCache.LogMsg($"in DoWork3 of MyService {workItem}");
                return true;
            });
        }

        public Task<bool> DoWork2(string workItem)
        {
            return new Task<bool>(() =>
            {
                MyRedisCache.LogMsg($"in DoWork2 of MyService {workItem}");
                return true;
            });
        }

        public bool DoWork(string workItem)
        {
            MyRedisCache.LogMsg($"in DoWork2 of MyService {workItem}");
            //subFunction();
            return true;
        }
    }
}
