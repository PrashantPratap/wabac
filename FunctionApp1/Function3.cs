using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Azure.WebJobs.ServiceBus;
using System.Threading.Tasks;

namespace FunctionApp1
{
    public static class Function3
    {
        [FunctionName("Function3")]
        public static async Task Run([EventHubTrigger("eventhubone", Connection = "hello")]string myEventHubMessage, TraceWriter log)
        {
            log.Info($"DLL C# Event Hub trigger function processed a message: {myEventHubMessage}");
            
        }
    }
}
