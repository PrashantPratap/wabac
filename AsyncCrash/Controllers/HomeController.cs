﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AsyncCrash.Controllers
{
    public class HomeController : Controller
    {
        static Exception myEx;
        protected override void OnException(ExceptionContext filterContext)
        {
            myEx = filterContext.Exception;
            base.OnException(filterContext);
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetOne()
        {
            JsonResult res = new JsonResult();
            res.JsonRequestBehavior = JsonRequestBehavior.AllowGet;

            var x = Session["one"] ;
            if (x == null)
            {
                x = DateTime.Now;
                Session["one"] = x;
            }
            res.Data = x.ToString(); ;
            return res;
        }

        public ActionResult About()
        {
                        ViewBag.Message = "Your application description page." + System.Diagnostics.Process.GetCurrentProcess().Id;
            if (myEx !=null)
            {
                ViewBag.Message = myEx.ToString();
            }


            return View();
        }

        private int slowFunc(int a, int b)
        {
           System.Threading.Thread.Sleep(5 * 60 * 1000);

            if(a ==b )
            {
                throw new Exception();
            }
            return a + b;
        }
        async public Task<ActionResult> Contact()
        {
            ViewBag.Message = "Your contact page." + System.Diagnostics.Process.GetCurrentProcess().Id;

            //var x = await Task<int>.Factory.StartNew(() => slowFunc(1, 1));

            Task<int>.Factory.StartNew(() => slowFunc(1, 1));
            //Task.Run(() => slowFunc(1, 1));
            

            return View();
        }
    }
}