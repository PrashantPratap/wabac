﻿using Microsoft.Azure.Services.AppAuthentication;
using Microsoft.Azure.KeyVault;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Net.Http.Headers;

namespace ManagedIdentitiesAppService.Controllers
{
    public class HomeController : Controller
    {
        
        public ActionResult Index()
        {
            return View();
        }

public ActionResult About()
{
    ViewBag.Message = "Your application description page." + User.Identity.Name; 

    return View();
}

static HttpClient client = new HttpClient();
        public ActionResult Contact()
        {

            var azureServiceTokenProvider = new AzureServiceTokenProvider();
            var accessTokenTask = azureServiceTokenProvider.GetAccessTokenAsync("https://adtry.azurewebsites.net/");//("86f27f56-4ac0-4916-b9e0-a9d097aba83b"); 

            accessTokenTask.Wait();
            var accessToken = accessTokenTask.Result;


            //var url = "https://wabaconead.azurewebsites.net/home/about";
            var url = "https://adtry.azurewebsites.net/home/about";

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            var responseTask = client.GetStringAsync(url);

            responseTask.Wait();
            var reponse = responseTask.Result;


            ViewBag.Message = "Your contact page." + accessToken + " HTML Response " + reponse;

            return View();
        }

        public ActionResult KeyVault()
        {
            var azureServiceTokenProvider = new AzureServiceTokenProvider();
            
            var kv = new KeyVaultClient(new KeyVaultClient.AuthenticationCallback(azureServiceTokenProvider.KeyVaultTokenCallback));

            var valueTask = kv.GetSecretAsync("https://wabackeyvault.vault.azure.net/secrets/mykey");
            valueTask.Wait();

            var value = valueTask.Result;

            ViewBag.Message = $"KEY-VALUE is {value.Value}"; 
            return View();
        }

    }
}