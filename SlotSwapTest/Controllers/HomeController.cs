﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SlotSwapTest.Controllers
{
    public class HomeController : Controller
    {
        
        public ActionResult Index()
        {
            ViewBag.Message = "Slot Swap test";
            return View();
        }

        public ActionResult About()
        {
            var host = System.Web.HttpContext.Current.Request.Url.Host;
            var slotname = Environment.GetEnvironmentVariable("SlotName");
            var db = Environment.GetEnvironmentVariable("SQLAZURECONNSTR_DB");

            ViewBag.Message = $"Host is <b>{host}</b> <br />  Slot name is <b>{slotname}</b> DB name is <b>{db}</b>  <br />  ProcessID is <b>{System.Diagnostics.Process.GetCurrentProcess().Id}</b> AppDomainID is <b>{AppDomain.CurrentDomain.Id}</b> ";

            
            return View();
        }
        

        public ActionResult Contact()
        {
            MvcApplication.InitText += "AppInit-";
            
            ViewBag.Message = "Your contact page.";

            return View();
        }

        //public ActionResult LoadTest(int id)
        //{
        //    System.Threading.Thread.Sleep(60 * 1000);

        //}



        //[HttpPost]
        //public ActionResult HitDatabase(string newtext, string consultant, DateTime date, string plantext, int contactid)
        //{
        //    //hit database to keep connection alive
        //    try
        //    {
        //        //
        //        //  http://social.technet.microsoft.com/wiki/contents/articles/4235.retry-logic-for-transient-failures-in-windows-azure-sql-database.aspx
        //        //  Retry logic for transient failures
        //        //
        //        int retry = 3;
        //        while (retry > 0)
        //        {
        //            try
        //            {
        //                // execute SQL cmd here
        //                break; // break out of the retry-while-loop
        //            }
        //            catch (SqlException sqlEx)
        //            {
        //                if (sqlEx.Number == -2) // SQL Timeout exception 
        //                {
        //                    retry--;
        //                    if (retry == 0)
        //                    {
        //                        // we tried three times and we still failed...log this exception
        //                        appInsights.trackException(sqlEx);
        //                        break; // break out of the retry-while-loop
        //                    }
        //                    else
        //                    {
        //                        // sleep for couple of seconds (then 4 seconds) before retrying
        //                        System.Threading.Thread.Sleep((4 / retry) * 1000);
        //                    }
        //                }
        //                else
        //                {
        //                    // not a timeout exception...log this exception
        //                    appInsights.trackException(sqlEx);
        //                    break; // break out of the retry-while-loop
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        //if keep alive fails
        //        appInsights.trackException(ex);
        //    }

        //    return Json(true, JsonRequestBehavior.AllowGet);
        //}


    }
}