﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AICPlusPlusTestWebsite.Models
{
    /*
    [
    {
        "time": "2017-09-14T17:24:59.884Z",
        "iKey": "f755f360-ddcf-48bb-aa37-f5d197f070ed",
        "name": "Microsoft.ApplicationInsights.f755f360ddcf48bbaa37f5d197f070ed.Pageview",
        "tags": {
            "ai.session.id": "9aWbo",
            "ai.device.id": "browser",
            "ai.device.type": "Browser",
            "ai.internal.sdkVersion": "javascript:1.0.11",
            "ai.user.id": "df6oK",
            "ai.operation.id": "kI+1o",
            "ai.operation.name": "/casetriagedesktop/casetriageapps/reportPerEng/jeffross/10"
        },
        "data": {
            "baseType": "PageviewData",
            "baseData": {
                "ver": 2,
                "name": "ReportPerEng",
                "url": "http://azuresdi02/casetriagedesktop/casetriageapps/reportPerEng/jeffross/10",
                "duration": "00:00:02.998"
            }
        }
    },
    {
        "time": "2017-09-14T17:24:59.885Z",
        "iKey": "f755f360-ddcf-48bb-aa37-f5d197f070ed",
        "name": "Microsoft.ApplicationInsights.f755f360ddcf48bbaa37f5d197f070ed.PageviewPerformance",
        "tags": {
            "ai.session.id": "9aWbo",
            "ai.device.id": "browser",
            "ai.device.type": "Browser",
            "ai.internal.sdkVersion": "javascript:1.0.11",
            "ai.user.id": "df6oK",
            "ai.operation.id": "kI+1o",
            "ai.operation.name": "/casetriagedesktop/casetriageapps/reportPerEng/jeffross/10"
        },
        "data": {
            "baseType": "PageviewPerformanceData",
            "baseData": {
                "ver": 2,
                "name": "ReportPerEng",
                "url": "http://azuresdi02/casetriagedesktop/casetriageapps/reportPerEng/jeffross/10",
                "duration": "00:00:02.998",
                "perfTotal": "00:00:02.998",
                "networkConnect": "00:00:00.013",
                "sentRequest": "00:00:02.666",
                "receivedResponse": "00:00:00.003",
                "domProcessing": "00:00:00.313"
            }
        }
    }
]*/

    //    public class Tags
    //    {
    //        public string ai.session.id { get; set; }
    //        public string __invalid_name__ai.device.id { get; set; }
    //        public string __invalid_name__ai.device.type { get; set; }
    //        public string __invalid_name__ai.internal.sdkVersion { get; set; }
    //    public string __invalid_name__ai.user.id { get; set; }
    //    public string __invalid_name__ai.operation.id { get; set; }
    //    public string __invalid_name__ai.operation.name { get; set; }
    //}

    public class BaseData
    {
        public int ver { get; set; }
        public string name { get; set; }
        public string url { get; set; }
        public string duration { get; set; }
        public string perfTotal { get; set; }
        public string networkConnect { get; set; }
        public string sentRequest { get; set; }
        public string receivedResponse { get; set; }
        public string domProcessing { get; set; }
    }

    public class Data
    {
        public string baseType { get; set; }
        public BaseData baseData { get; set; }
    }

    public class AIModel
    {
        public DateTime time { get; set; }
        public string iKey { get; set; }
        public string name { get; set; }
        // public Tags tags { get; set; }
        public Data data { get; set; }
    }

}
