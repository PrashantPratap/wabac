﻿using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace FAppARM
{
    class Program
    {
        static string tenantId = "72f988bf-86f1-41af-91ab-2d7cd011db47";
        static string username = "http://wabacuser";
        static string password = "Micr0s0ft";
        static string token = string.Empty;
        static string subscription = "25d360ff-1859-4a27-aada-a216484284b3";
        static string resourceGroup = "highcpu";
        static string appServicePlan = "highcpu";

        static HttpClient httpClient = new HttpClient();
        static void Main(string[] args)
        {
            var t = GetToken();
            t.Wait();
            var url = $"https://management.azure.com/subscriptions/{subscription}/resourceGroups/{resourceGroup}/providers/Microsoft.Web/serverfarms/{appServicePlan}?api-version=2016-09-01";
            var t2 = Get(url);
            t2.Wait();
            var t3 = Put(url, "{ \"location\":\"South Central US\",\"Sku\":{\"Name\":\"S1\"}}");
            t3.Wait();
        }

        public static async Task<string> GetToken()
        {
            string url = "https://login.windows.net/" + tenantId;
            var authContext = new AuthenticationContext(url);
            var credential = new ClientCredential(username, password);
            var t = await authContext.AcquireTokenAsync("https://management.azure.com/", credential);
            token = t.AccessToken;
            return token;
        }

        static async Task<string> Get(string url)
        {
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            return await httpClient.GetStringAsync(url);
        }

        static async Task<string> Put(string url, string jsonData)
        {
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            StringContent postData = new StringContent(jsonData, System.Text.Encoding.UTF8, "application/json");
            var t = await httpClient.PutAsync(url, postData);
            return await t.Content.ReadAsStringAsync();
        }

        static async Task<string> Post(string url, string jsonData)
        {
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            StringContent postData = new StringContent(jsonData, System.Text.Encoding.UTF8, "application/json");
            var t = await httpClient.PostAsync(url, postData);
            return await t.Content.ReadAsStringAsync();
        }
    }
}
